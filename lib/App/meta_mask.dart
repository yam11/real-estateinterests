import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:walletconnect_dart/walletconnect_dart.dart';
import 'package:walletconnect_flutter_v2/walletconnect_flutter_v2.dart';

class MetaMask {
  BuildContext context;

  Web3Wallet? web3Wallet;

  MetaMask({required this.context});

  setWallet() async {
    web3Wallet = await Web3Wallet.createInstance(
      relayUrl:
          'wss://bridge.walletconnect.com', // The relay websocket URL, leave blank to use the default
      projectId: '123',
      metadata: PairingMetadata(
        name: 'Wallet (Responder)',
        description: 'A wallet that can be requested to sign transactions',
        url: 'https://walletconnect.com',
        icons: ['https://avatars.githubusercontent.com/u/37784886'],
      ),
    );
    print("what is wwwwwww $web3Wallet");
  }

  kadenaSignRequestHandler(String topic, dynamic parameters) async {
    final parsedResponse = parameters;

    bool userApproved = await showDialog(
      // This is an example, you will have to make your own changes to make it work.
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Sign Transaction'),
          content: SizedBox(
            width: 300,
            height: 350,
            child: Text(parsedResponse.toString()),
          ),
          actions: [
            ElevatedButton(
              onPressed: () => Navigator.pop(context, true),
              child: Text('Accept'),
            ),
            ElevatedButton(
              onPressed: () => Navigator.pop(context, false),
              child: Text('Reject'),
            ),
          ],
        );
      },
    );

    if (userApproved) {
      return 'Signed!';
    } else {
      throw Errors.getSdkError(Errors.USER_REJECTED_SIGN);
    }
  }

  getWallet() {
    web3Wallet!.registerRequestHandler(
      chainId: 'kadena',
      method: 'kadena_sign',
      handler: kadenaSignRequestHandler,
    );
  }
}

Future<void> launch(Uri url) async {
  try {
    await canLaunchUrl(url);
    await launchUrl(url, mode: LaunchMode.externalApplication);
  } catch (e) {
    print("whet is e $e");

    Fluttertoast.showToast(
        msg: "Could Not Launch ",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 3,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
