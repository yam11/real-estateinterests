part of 'app_theme_bloc.dart';

@immutable
class ChangeTheme {
  final bool isDarkMode;
  const ChangeTheme({required this.isDarkMode});
}
