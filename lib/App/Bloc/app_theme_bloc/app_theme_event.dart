part of 'app_theme_bloc.dart';

@immutable
abstract class AppThemeEvent {}
class InitTheme extends AppThemeEvent{}
class ChangeThemeToDark extends AppThemeEvent{}

class ChangeThemeToLight extends AppThemeEvent{}
