import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:senior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';

part 'app_theme_event.dart';
part 'app_theme_state.dart';

class AppThemeBloc extends Bloc<AppThemeEvent, ChangeTheme> {
  AppThemeBloc()
      : super(ChangeTheme(isDarkMode: AppSharedPreferences.getDarkTheme)) {
    on<AppThemeEvent>((event, emit) async {
      if (event is InitTheme) {
        emit(ChangeTheme(isDarkMode: AppSharedPreferences.getDarkTheme));
      }
      if (event is ChangeThemeToDark) {
        await AppSharedPreferences.saveDarkTheme(true);
        emit(ChangeTheme(isDarkMode: AppSharedPreferences.getDarkTheme));
      }
      if (event is ChangeThemeToLight) {
        await AppSharedPreferences.removeDarkTheme();
        emit(ChangeTheme(isDarkMode: AppSharedPreferences.getDarkTheme));
      }
    });
  }
}
