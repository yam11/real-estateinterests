part of 'app_language_bloc.dart';

@immutable
class ChangeLanguage {
  final Locale locale;

  const ChangeLanguage({required this.locale});
}
