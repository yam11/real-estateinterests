import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:senior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';

part 'app_language_event.dart';
part 'app_language_state.dart';

class AppLanguageBloc extends Bloc<AppLanguageEvent, ChangeLanguage> {
  AppLanguageBloc()
      : super(ChangeLanguage(locale: Locale(AppSharedPreferences.getArLang))) {
    on<AppLanguageEvent>((event, emit) async {
      if (event is InitLanguage) {
        emit(ChangeLanguage(locale: Locale(AppSharedPreferences.getArLang)));
      }
      if (event is ChangeLanguageToAr) {
        await AppSharedPreferences.saveArLang('ar');
        emit(ChangeLanguage(locale: Locale(AppSharedPreferences.getArLang)));
      }
      if (event is ChangeLanguageToEn) {
        await AppSharedPreferences.removeArLang();
        emit(ChangeLanguage(locale: Locale(AppSharedPreferences.getArLang)));
      }
    });
  }
}
