part of 'scroll_bloc.dart';

@immutable
abstract class ScrollState {}

class ScrollInitial extends ScrollState {}
class ScrollListener extends ScrollState{}
