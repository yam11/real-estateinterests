import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'scroll_event.dart';
part 'scroll_state.dart';

class ScrollBloc extends Bloc<ScrollEvent, ScrollState> {
    bool appBar = true;

  ScrollBloc() : super(ScrollInitial()) {
    on<ScrollEvent>((event, emit) {
       if (event is ShowAppBar) {
        appBar = true;
        emit(ScrollListener());
      }
      if (event is HideAppBar) {
        appBar = false;
        emit(ScrollListener());
      }
    });
  }
}
