part of 'scroll_bloc.dart';

@immutable
abstract class ScrollEvent {}
class HideAppBar extends ScrollEvent{}
class ShowAppBar extends ScrollEvent{}