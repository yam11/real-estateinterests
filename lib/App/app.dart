import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:senior/App/Bloc/app_language_bloc/app_language_bloc.dart';
import 'package:senior/App/Bloc/app_theme_bloc/app_theme_bloc.dart';
import 'package:senior/App/Bloc/connectivity_bloc/connectivity_bloc.dart';
import 'package:senior/App/Bloc/scroll_bloc/scroll_bloc.dart';
import 'package:senior/App/app_localizations.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:senior/Feature/Home/Bloc/home_bloc/home_bloc.dart';
import 'package:senior/Feature/Main/Presentation/Pages/main_page.dart';
import 'package:senior/Feature/Splash/Presentation/Pages/splash_page.dart';
import 'package:sizer/sizer.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) => MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (_) => AppLanguageBloc()..add(InitLanguage()),
            ),
            BlocProvider(
              create: (_) => AppThemeBloc()..add(InitTheme()),
            ),
            BlocProvider(
              create: (context) => ConnectivityBloc(),
            ),
            BlocProvider(
              create: (_) => ScrollBloc(),
            ),
          ],
          child: BlocBuilder<AppThemeBloc, ChangeTheme>(
            builder: (context, state) {
              return BlocBuilder<AppLanguageBloc, ChangeLanguage>(
                builder: (context, state) {
                  return MaterialApp(
                    builder: FToastBuilder(),
                    debugShowCheckedModeBanner: false,
                    locale: Locale(AppSharedPreferences.getArLang),
                    supportedLocales: const [Locale('en', 'US'), Locale('ar')],
                    localizationsDelegates: const [
                      Applocalizations.delegate,
                      GlobalMaterialLocalizations.delegate,
                      GlobalWidgetsLocalizations.delegate,
                      GlobalCupertinoLocalizations.delegate
                    ],
                    localeResolutionCallback: (deviceLocale, supportedLocales) {
                      for (var locale in supportedLocales) {
                        if (deviceLocale != null &&
                            deviceLocale.languageCode == locale.languageCode) {
                          return deviceLocale;
                        }
                      }
                      return supportedLocales.first;
                    },
                    home: SplashPage(),
                  );
                },
              );
            },
          )),
    );
  }
}
