import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:page_transition/page_transition.dart';
import 'package:senior/App/Bloc/app_language_bloc/app_language_bloc.dart';
import 'package:senior/App/Bloc/app_theme_bloc/app_theme_bloc.dart';
import 'package:senior/App/app_localizations.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:senior/Feature/Setting/Presentation/Widgets/language_button_widget.dart';
import 'package:senior/Feature/Setting/Presentation/Widgets/theme_button_widget.dart';
import 'package:senior/Feature/Splash/Presentation/Pages/splash_page.dart';
import 'package:sizer/sizer.dart';

class SettingPage extends StatelessWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppThemeBloc, ChangeTheme>(
      builder: (context, state) {
        return Center(
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(
                parent: AlwaysScrollableScrollPhysics()),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
                  child: Row(
                    children: [
                      Expanded(
                        child: MaterialButton(
                          onPressed: () {},
                          color: AppColors.getPrimarySeconedary(),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.w),
                              side: BorderSide(
                                  width: 1,
                                  color: AppColors.getPrimarySeconedary())),
                          child: Text(
                            "Edit Profile".tr(context),
                            style: TextStyle(
                              fontSize: 16.sp,
                              fontWeight: FontWeight.bold,
                              color: AppColors.getBlackWhite(),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
                  child: Row(
                    children: [
                      Expanded(
                        child: MaterialButton(
                          onPressed: () {},
                          color: AppColors.getPrimarySeconedary(),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.w),
                              side: BorderSide(
                                  width: 1,
                                  color: AppColors.getPrimarySeconedary())),
                          child: Text(
                            "Edit Password".tr(context),
                            style: TextStyle(
                              fontSize: 16.sp,
                              fontWeight: FontWeight.bold,
                              color: AppColors.getBlackWhite(),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Language".tr(context),
                        style: TextStyle(
                            color: AppColors.primaryColor,
                            fontSize: 16.sp,
                            fontWeight: FontWeight.bold),
                      ),
                      Row(
                        children: [
                          LanguageButton(
                            title: 'English'.tr(context),
                            onPress: () {
                              context
                                  .read<AppLanguageBloc>()
                                  .add(ChangeLanguageToEn());
                            },
                            isPressed: !AppSharedPreferences.hasArLang,
                          ),
                          SizedBox(
                            width: 1.w,
                          ),
                          LanguageButton(
                            title: 'Arabic'.tr(context),
                            onPress: () {
                              context
                                  .read<AppLanguageBloc>()
                                  .add(ChangeLanguageToAr());
                            },
                            isPressed: AppSharedPreferences.hasArLang,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Mode".tr(context),
                        style: TextStyle(
                            color: AppColors.primaryColor,
                            fontSize: 16.sp,
                            fontWeight: FontWeight.bold),
                      ),
                      Row(
                        children: [
                          ThemeButton(
                            title: 'Dark'.tr(context),
                            onPress: () {
                              context
                                  .read<AppThemeBloc>()
                                  .add(ChangeThemeToDark());
                            },
                            isPressed: AppSharedPreferences.hasDarkTheme,
                          ),
                          SizedBox(
                            width: 1.w,
                          ),
                          ThemeButton(
                            title: 'Light'.tr(context),
                            onPress: () {
                              context
                                  .read<AppThemeBloc>()
                                  .add(ChangeThemeToLight());
                            },
                            isPressed: !AppSharedPreferences.hasDarkTheme,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
