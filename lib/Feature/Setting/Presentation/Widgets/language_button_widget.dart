import 'package:flutter/material.dart';
import 'package:senior/Core/Constants/app_colors.dart';

import 'package:sizer/sizer.dart';

class LanguageButton extends StatelessWidget {
  final String title;
  final bool isPressed;
  final Function onPress;

  const LanguageButton(
      {Key? key,
      required this.title,
      required this.isPressed,
      required this.onPress})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onPress();
      },
      borderRadius:   BorderRadius.circular(5.w), 
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 1.h, horizontal: 2.w),
        margin: EdgeInsets.symmetric(horizontal: 1.w),
        decoration: BoxDecoration(
            border: Border.all(
                color: isPressed
                    ? AppColors.getSeconedaryPrimary()
                    : AppColors.getPrimarySeconedary(),
                width: 2),
            borderRadius: BorderRadius.circular(5.w),
            color: AppColors.getPrimarySeconedary(),
            boxShadow: [
              BoxShadow(
                color: AppColors.boxShadowColor(),
                blurRadius: 5,
                offset: const Offset(0, 3),
              )
            ]),
        child: Center(
          child: Text(
            title,
            style: TextStyle(
                color: AppColors.getBlackWhite(),
                fontSize: 10.sp,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
