import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:map_launcher/map_launcher.dart' as maplauncher;
import 'package:senior/App/Bloc/scroll_bloc/scroll_bloc.dart';
import 'package:senior/App/app_localizations.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Widgets/app_bar_widget.dart';
import 'package:senior/Core/Widgets/loading_widget.dart';
import 'package:senior/Feature/AddNewPost/Bloc/map_bloc/map_bloc.dart';
import 'package:sizer/sizer.dart';

class MapPage extends StatelessWidget {
  Function(LatLng)? selectedLocation;

  MapPage({this.selectedLocation, super.key});

  MapBloc mapBloc = MapBloc();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => mapBloc,
        child: Scaffold(
            floatingActionButton: BlocBuilder<MapBloc, GetLocationState>(
              builder: (context, state) {
                return mapBloc.selectedLocation != null
                    ? FloatingActionButton.extended(
                        backgroundColor: AppColors.getPrimarySeconedary(),
                        foregroundColor: Colors.black,
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        label: Text(
                          "Done",
                          style: TextStyle(
                              color: AppColors.getBlackWhite(),
                              fontSize: 15.sp,
                              fontWeight: FontWeight.bold),
                        ),
                      )
                    : const SizedBox();
              },
            ),
            appBar: PreferredSize(
                preferredSize: Size.fromHeight(AppBar().preferredSize.height),
                child: SafeArea(
                  child: AppBarWidget(
                    buttonBack: true,
                    title: "Select the location on map".tr(context),
                  ),
                )),
            body: BlocBuilder<MapBloc, GetLocationState>(
              builder: (context, state) {
                print(mapBloc.selectedLocation);
                if (state.isLoading) {
                  return Center(
                    child: LoadingWidget(),
                  );
                } else {
                  return GoogleMap(
                    gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>{
                      Factory<OneSequenceGestureRecognizer>(
                        () => EagerGestureRecognizer(),
                      ),
                    },
                    mapType: MapType.normal,
                    initialCameraPosition: mapBloc.initialSettings,
                    // myLocationEnabled: true,
                    onMapCreated: (GoogleMapController controller) {
                      // myMarker.add(Marker(
                      //     consumeTapEvents: true,
                      //     draggable: true,
                      //     onDragEnd: (value) {},
                      //     onTap: () async {
                      //       try {
                      //         if ((await maplauncher.MapLauncher.isMapAvailable(
                      //             maplauncher.MapType.google))!) {
                      //           await maplauncher.MapLauncher.showMarker(
                      //             mapType: maplauncher.MapType.google,
                      //             coords: maplauncher.Coords(
                      //                 double.parse(widget.lat), double.parse(widget.lang)),
                      //             title: 'trip location',
                      //           );
                      //         }
                      //         print('hello world map 2');
                      //       } catch (error) {
                      //         print('hello world map 2 error $error');
                      //       }
                      //     },
                      //     markerId: MarkerId("1"),
                      //     position: LatLng(31.1313, 29.9773)));

                      mapBloc.googleMapController.complete(controller);
                    },
                    onTap: (latLng) {
                      mapBloc.add(SelectLocaton(location: latLng));
                      selectedLocation!(latLng);

                      print("what is the location $selectedLocation");
                    },
                    markers: mapBloc.selectedLocation == null
                        ? {}
                        : {
                            Marker(
                              markerId: MarkerId('selected_location'),
                              position: mapBloc.selectedLocation!,
                            ),
                          },
                  );
                }
              },
            )));
  }
}
