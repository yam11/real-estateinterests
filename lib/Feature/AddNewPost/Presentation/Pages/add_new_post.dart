import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:senior/App/app_localizations.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:senior/Core/Widgets/app_bar_widget.dart';
import 'package:senior/Core/Widgets/coustom_text_falid.dart';
import 'package:senior/Feature/AddNewPost/Bloc/add_new_post_bloc/add_new_post_bloc.dart';
import 'package:senior/Feature/AddNewPost/Presentation/Pages/map_page.dart';
import 'package:sizer/sizer.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class AddNewPostWidget extends StatelessWidget {
  AddNewPostWidget({super.key});
  LatLng? selectedLocation;
  TextEditingController descriptionController = TextEditingController();
  TextEditingController categoryController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  TextEditingController priceController = TextEditingController();

  FocusNode descriptionNode = FocusNode();
  FocusNode categoryNode = FocusNode();
  FocusNode priceNode = FocusNode();
  FocusNode locationNode = FocusNode();

  AddNewPostBloc addNewPostBloc = AddNewPostBloc();
  @override
  Widget build(BuildContext context) {
    print(selectedLocation);
    return BlocProvider(
      create: (context) => addNewPostBloc,
      child: Directionality(
        textDirection: AppSharedPreferences.getArLang == "ar"
            ? TextDirection.rtl
            : TextDirection.ltr,
        child: Scaffold(
          backgroundColor: AppColors.getBlackLightWhite(),
          extendBody: true,
          extendBodyBehindAppBar: true,
          appBar: PreferredSize(
              preferredSize: Size.fromHeight(AppBar().preferredSize.height),
              child: SafeArea(
                child: AppBarWidget(
                  buttonBack: true,
                  title: "Add New Post".tr(context),
                ),
              )),
          body: ListView(
            physics: const BouncingScrollPhysics(
                parent: AlwaysScrollableScrollPhysics()),
            children: [
              SizedBox(
                height: 2.h,
              ),
              BlocBuilder<AddNewPostBloc, InitState>(
                builder: (context, state) {
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 2.w),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Images".tr(context),
                              style: TextStyle(
                                  fontSize: 18.sp,
                                  fontWeight: FontWeight.bold,
                                  color: AppColors.getPrimarySeconedary()),
                            ),
                            Row(
                              children: [
                                IconButton(
                                    onPressed: () {
                                      // if (addNewPostCubit
                                      //         .imageProvidersList.length ==
                                      //     5) {
                                      //   Fluttertoast.showToast(
                                      //       msg: "You can only pick 5 images"
                                      //           .i18n(),
                                      //       toastLength: Toast.LENGTH_LONG,
                                      //       gravity: ToastGravity.TOP,
                                      //       timeInSecForIosWeb: 5,
                                      //       backgroundColor: Colors.red,
                                      //       textColor: AppColors.greyColor,
                                      //       fontSize: 16.0);
                                      //   return;
                                      // } else {
                                      addNewPostBloc
                                          .add(PickSingleImageFromCamera());
                                      // }
                                    },
                                    icon: Icon(
                                      Icons.photo_camera_outlined,
                                      color: AppColors.getPrimarySeconedary(),
                                    )),
                                IconButton(
                                    onPressed: () {
                                      // if (addNewPostCubit
                                      //         .imageProvidersList.length ==
                                      //     5) {
                                      //   Fluttertoast.showToast(
                                      //       msg: "You can only pick 5 images"
                                      //           .i18n(),
                                      //       toastLength: Toast.LENGTH_LONG,
                                      //       gravity: ToastGravity.TOP,
                                      //       timeInSecForIosWeb: 5,
                                      //       backgroundColor: Colors.red,
                                      //       textColor: AppColors.greyColor,
                                      //       fontSize: 16.0);
                                      //   return;
                                      // } else {
                                      addNewPostBloc
                                          .add(PickSingleImageFromGallery());
                                      // }
                                    },
                                    icon: Icon(
                                      Icons.photo,
                                      color: AppColors.getPrimarySeconedary(),
                                    )),
                              ],
                            )
                          ],
                        ),
                        addNewPostBloc.images.isNotEmpty
                            ? Column(
                                children: [
                                  CarouselSlider.builder(
                                      itemCount: addNewPostBloc.images.length,
                                      itemBuilder: (context, index, realIndex) {
                                        return InkWell(
                                          onTap: () {
                                            // Navigator.of(context).push(
                                            //     MaterialPageRoute(
                                            //         builder: (_) =>
                                            //             ViewPostImagesWidget(
                                            //               items: addNewPostCubit
                                            //                 .imageProvidersList.length,
                                            //               image: addNewPostCubit
                                            //                   .images,
                                            //               activeIndex: index,
                                            //             )));
                                          },
                                          child: Container(
                                            margin: EdgeInsets.symmetric(
                                                horizontal: 2.w),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: 1,
                                                    color:
                                                        AppColors.blackColor),
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                image: DecorationImage(
                                                    image: FileImage(
                                                        addNewPostBloc
                                                            .images[index]),
                                                    fit: BoxFit.contain)),
                                            child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    children: [
                                                      // IconButton(
                                                      //     onPressed: () {
                                                      //       addNewPostCubit
                                                      //           .cropImage();
                                                      //     },
                                                      //     icon: Icon(
                                                      //       Icons.edit,
                                                      //       size: 5.w,
                                                      //       color: AppColors
                                                      //           .primaryColor,
                                                      //     )),
                                                      IconButton(
                                                          onPressed: () {
                                                            addNewPostBloc.add(
                                                                RemoveImage(
                                                                    index:
                                                                        index));
                                                          },
                                                          icon: Icon(
                                                            Icons.delete,
                                                            size: 5.w,
                                                            color: AppColors
                                                                .primaryColor,
                                                          ))
                                                    ],
                                                  )
                                                ]),
                                          ),
                                        );
                                      },
                                      options: CarouselOptions(
                                        enableInfiniteScroll: false,
                                        height: 20.h,
                                        autoPlay: false,
                                        viewportFraction: 1,
                                        onPageChanged: (index, reason) {
                                          addNewPostBloc
                                              .add(ChangeIndex(index: index));
                                        },
                                      )),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 5.w, vertical: 1.h),
                                        child: AnimatedSmoothIndicator(
                                            activeIndex:
                                                addNewPostBloc.activeIndex,
                                            count: addNewPostBloc.images.length,
                                            effect: const WormEffect(
                                                dotHeight: 10,
                                                dotWidth: 10,
                                                dotColor: AppColors.greyColor,
                                                activeDotColor:
                                                    AppColors.primaryColor)),
                                      ),
                                    ],
                                  )
                                ],
                              )
                            : Container(
                                // margin: EdgeInsets.symmetric(horizontal: 12),
                                height: 20.h,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  color: AppColors.greyColor,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Text(
                                  "Select your images".tr(context),
                                  style: TextStyle(
                                      color: AppColors.blackColor,
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.bold),
                                )),
                      ],
                    ),
                  );
                },
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Text(
                          "Description".tr(context),
                          style: TextStyle(
                              fontSize: 18.sp,
                              fontWeight: FontWeight.bold,
                              color: AppColors.getPrimarySeconedary()),
                        )),
                      ],
                    ),
                    CustomTextField(
                      fillColor: AppColors.getBlackWhite(),
                      // hintText: "Password",
                      maxLine: 5,
                      focusNode: descriptionNode,
                      textInputAction: TextInputAction.next,
                      textInputType: TextInputType.text,
                      controller: descriptionController,
                      isValidator: true,
                      validatorMessage: "Enter Your Password",
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Text(
                          "Category".tr(context),
                          style: TextStyle(
                              fontSize: 18.sp,
                              fontWeight: FontWeight.bold,
                              color: AppColors.getPrimarySeconedary()),
                        )),
                      ],
                    ),
                    CustomTextField(
                      fillColor: AppColors.getBlackWhite(),
                      // hintText: "Password",
                      maxLine: 1,
                      // focusNode: categoryNode,
                      textInputAction: TextInputAction.next,
                      textInputType: TextInputType.text,
                      controller: categoryController,
                      isValidator: true,
                      validatorMessage: "Enter Your Password",
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Text(
                          "Price".tr(context),
                          style: TextStyle(
                              fontSize: 18.sp,
                              fontWeight: FontWeight.bold,
                              color: AppColors.getPrimarySeconedary()),
                        )),
                      ],
                    ),
                    CustomTextField(
                      fillColor: AppColors.getBlackWhite(),
                      // hintText: "Password",
                      maxLine: 1,
                      focusNode: priceNode,
                      textInputAction: TextInputAction.next,
                      textInputType: TextInputType.text,
                      controller: priceController,
                      isValidator: true,
                      validatorMessage: "Enter Your Password",
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Text(
                          "Location".tr(context),
                          style: TextStyle(
                              fontSize: 18.sp,
                              fontWeight: FontWeight.bold,
                              color: AppColors.getPrimarySeconedary()),
                        )),
                      ],
                    ),
                    CustomTextField(
                      fillColor: AppColors.getBlackWhite(),
                      // hintText: "Password",
                      focusNode: locationNode,
                      textInputAction: TextInputAction.next,
                      textInputType: TextInputType.text,
                      controller: locationController,
                      isValidator: true,
                      validatorMessage: "Enter Your Password",
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    MaterialButton(
                        shape: RoundedRectangleBorder(
                            side: BorderSide(
                                width: 1,
                                color: AppColors.getPrimarySeconedary())),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => MapPage(
                              selectedLocation: (latLng) {
                                selectedLocation = latLng;
                              },
                            ),
                          ));
                        },
                        child: Text(
                          "Open map".tr(context),
                          style: TextStyle(
                              color: AppColors.getPrimarySeconedary(),
                              fontSize: 16.sp,
                              fontWeight: FontWeight.bold),
                        )),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                child: MaterialButton(
                    color: AppColors.getPrimarySeconedary(),
                    shape: RoundedRectangleBorder(
                        side: BorderSide(
                            width: 1, color: AppColors.getPrimarySeconedary())),
                    onPressed: () {
                      addNewPostBloc.add(AddNewPost(
                          location: locationController.text,
                          price: priceController.text,
                          lat: selectedLocation!.latitude,
                          log: selectedLocation!.longitude));
                    },
                    child: Text(
                      "Add New Post".tr(context),
                      style: TextStyle(
                          color: AppColors.getBlackWhite(),
                          fontSize: 16.sp,
                          fontWeight: FontWeight.bold),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
