part of 'map_bloc.dart';

@immutable


class GetLocationState  {
  bool isLoading;
  GetLocationState({required this.isLoading});
}
