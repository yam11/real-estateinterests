part of 'map_bloc.dart';

@immutable
abstract class MapEvent {}

class RequestPermission extends MapEvent {}

class GetLocation extends MapEvent {}

class SelectLocaton extends MapEvent {
  LatLng location;
  SelectLocaton({required this.location});
}
