import 'dart:async';
import 'package:location/location.dart' as loc;

import 'package:bloc/bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';
import 'package:permission_handler/permission_handler.dart';

part 'map_event.dart';
part 'map_state.dart';

class MapBloc extends Bloc<MapEvent, GetLocationState> {
  LatLng? selectedLocation;
  Completer<GoogleMapController> googleMapController = Completer();
  final loc.Location location = loc.Location();
  late final loc.LocationData locationResult;
  late CameraPosition initialSettings;

  MapBloc() : super(GetLocationState(isLoading: true)) {
    on<RequestPermission>((event, emit) async {
      var status = await Permission.location.request();
      if (status.isGranted) {
        print('done');
      } else if (status.isDenied) {
        add(RequestPermission());
      } else if (status.isPermanentlyDenied) {
        openAppSettings();
      }
    });
    on<GetLocation>((event, emit) async {
      locationResult = await location.getLocation();
      initialSettings = CameraPosition(
        target: LatLng(locationResult.latitude!, locationResult.longitude!),
        zoom: 16.0,
      );
      emit(GetLocationState(isLoading: false));
    });

    on<SelectLocaton>((event, emit) async {
      selectedLocation = event.location;

      emit(GetLocationState(isLoading: false));
    });
    add(RequestPermission());
    add(GetLocation());
  }
}
