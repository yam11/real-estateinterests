part of 'add_new_post_bloc.dart';

@immutable
abstract class InitState {}

class AddPostState extends InitState {
  String? message;

  bool isLoading;
  AddPostState({ this.message, required this.isLoading});
}

class ErrorState extends InitState {
  final String message;

  ErrorState({required this.message});
}
