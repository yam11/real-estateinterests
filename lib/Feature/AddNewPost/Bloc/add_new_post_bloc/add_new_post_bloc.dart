import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:senior/Core/Api/ExceptionsHandle.dart';
import 'package:senior/Core/Api/Network.dart';
import 'package:senior/Core/Api/Urls.dart';
import 'package:image_picker/image_picker.dart';

part 'add_new_post_event.dart';
part 'add_new_post_state.dart';

class AddNewPostBloc extends Bloc<AddNewPostEvent, InitState> {
  final List<File> images = [];
  int activeIndex = 0;
  AddNewPostBloc() : super(AddPostState(isLoading: false)) {
    on<AddNewPost>((event, emit) async {
      emit(AddPostState(isLoading: true));
      try {
        await Network.postData(url: "${Urls.baseUrl}realEstate", data: {
          "location": event.location,
          "description":
              "المنزل يتكون من ثلاث غرف نوم وصالون، له اطلالة جميلة تدخل له الشمس من جهة الشرق، المنزل طابق أول والبناء مزود بمصعد",
          "images": [
            "https://th.bing.com/th/id/OIP.azPaffWcZJ7XIKXDPaivIwHaFj?w=209&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7",
            "https://th.bing.com/th/id/OIP.Tp_G6Ifh8j_j64NW4wxtyQHaEt?w=290&h=185&c=7&r=0&o=5&dpr=1.3&pid=1.7"
          ],
          "category": "منزل",
          "label": "منزل",
          "price": event.price,
          "featured": true,
          "coordinates": {
            "type": "Point",
            "coordinates": [event.lat, event.log]
          },
        });
        emit(
            AddPostState(isLoading: false, message: "Succes to add new post "));
      } catch (error) {
        if (error is DioError) {
          emit(ErrorState(message: exceptionsHandle(error: error)));
        } else {
          emit(ErrorState(message: "error"));
        }
      }
    });
    on<PickSingleImageFromGallery>((event, emit) async {
      ImagePicker picker = ImagePicker();
      final file = await picker.pickImage(source: ImageSource.gallery);

      images.add(File(file!.path));
      emit(AddPostState(isLoading: false));
    });
    on<PickSingleImageFromCamera>((event, emit) async {
      ImagePicker picker = ImagePicker();
      final file = await picker.pickImage(source: ImageSource.camera);

      images.add(File(file!.path));

      emit(AddPostState(isLoading: false));
    });
    on<RemoveImage>((event, emit) async {
      images.removeAt(event.index);

      emit(AddPostState(isLoading: false));
    });
    on<ChangeIndex>((event, emit) async {
      activeIndex = event.index;

      emit(AddPostState(isLoading: false));
    });
  }
}
