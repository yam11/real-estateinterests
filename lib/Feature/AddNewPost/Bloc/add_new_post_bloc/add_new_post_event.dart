part of 'add_new_post_bloc.dart';

@immutable
abstract class AddNewPostEvent {}

class AddNewPost extends AddNewPostEvent {
  double lat;
  double log;
  String price;
  String location;

  AddNewPost(
      {required this.location,
      required this.price,
      required this.lat,
      required this.log});
}

class RemoveImage extends AddNewPostEvent {
  int index;
  RemoveImage({required this.index});
}

class ChangeIndex extends AddNewPostEvent {
  int index;
  ChangeIndex({required this.index});
}

class PickSingleImageFromGallery extends AddNewPostEvent {}

class PickSingleImageFromCamera extends AddNewPostEvent {}
