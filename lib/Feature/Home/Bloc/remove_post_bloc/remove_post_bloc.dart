import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:senior/Core/Api/ExceptionsHandle.dart';
import 'package:senior/Core/Api/Network.dart';
import 'package:senior/Core/Api/Urls.dart';
import 'package:senior/Feature/Home/Models/all_posts_model.dart';

part 'remove_post_event.dart';
part 'remove_post_state.dart';

class RemovePostBloc extends Bloc<RemovePostEvent, RemovePostState> {
  RemovePostBloc() : super(RemovePostInitial()) {
    on<RemovePostEvent>((event, emit) {
      // TODO: implement event handler
    });
    on<RemovePost>((event, emit) async {
      emit(PostRemoveState(isLoading: true));
      try {
        await Network.deleteData(
          url: "${Urls.getAllPosts}/${event.postId}",
        );
        event.realEstate.removeAt(event.index);
        emit(PostRemoveState(isLoading: false));
      } catch (error) {
        if (error is DioError) {
          if (error.response!.statusCode == 500) {
            emit(ErrorState(message: exceptionsHandle(error: error)));
          }
          emit(ErrorState(message: exceptionsHandle(error: error)));
        } else {
          emit(ErrorState(message: error.toString()));
        }
      }
    });
  }
}
