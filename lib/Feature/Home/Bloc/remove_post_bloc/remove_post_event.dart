part of 'remove_post_bloc.dart';

@immutable
abstract class RemovePostEvent {}

class RemovePost extends RemovePostEvent {
  String postId;
  List<RealEstate> realEstate;
  int index;
  RemovePost({required this.index,required this.postId, required this.realEstate});
}
