part of 'remove_post_bloc.dart';

@immutable
abstract class RemovePostState {}

class RemovePostInitial extends RemovePostState {}

class PostRemoveState extends RemovePostState {
  bool isLoading;
  PostRemoveState({required this.isLoading});
}

class ErrorState extends RemovePostState {
  String message;
  ErrorState({required this.message});
}
