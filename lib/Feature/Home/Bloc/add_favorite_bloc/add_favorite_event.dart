part of 'add_favorite_bloc.dart';

@immutable
abstract class AddFavoriteEvent {}

class AddFavorite extends AddFavoriteEvent {
  final String postId;
  AddFavorite({required this.postId});
}
class RemoveFavorite extends AddFavoriteEvent {
  final String postId;
  RemoveFavorite({required this.postId});
}
