import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:senior/Core/Api/ExceptionsHandle.dart';
import 'package:senior/Core/Api/Network.dart';
import 'package:senior/Core/Api/Urls.dart';

part 'add_favorite_event.dart';
part 'add_favorite_state.dart';

class AddFavoriteBloc extends Bloc<AddFavoriteEvent, AddFavoriteState> {
  AddFavoriteBloc() : super(AddFavoriteState()) {
    on<AddFavorite>((event, emit) async {
      emit(AddFavoriteState(isLoading: true));
      try {
        await Network.postData(
          url: "${Urls.favorites}/${event.postId}",
        );

        emit(AddFavoriteState(isLoading: false));
      } catch (error) {
        if (error is DioError) {
          if (error.response!.statusCode == 500) {
            emit(AddFavoriteState(
                isLoading: false, message: exceptionsHandle(error: error)));
          }
          emit(AddFavoriteState(
              isLoading: false, message: exceptionsHandle(error: error)));
        } else {
          emit(AddFavoriteState(isLoading: false, message: error.toString()));
        }
      }
    });

    on<RemoveFavorite>((event, emit) async {
      emit(AddFavoriteState(isLoading: true));
      try {
        await Network.deleteData(
          url: "${Urls.favorites}/${event.postId}",
        );

        emit(AddFavoriteState(isLoading: false));
      } catch (error) {
        if (error is DioError) {
          if (error.response!.statusCode == 500) {
            emit(AddFavoriteState(
                isLoading: false, message: exceptionsHandle(error: error)));
          }
          emit(AddFavoriteState(
              isLoading: false, message: exceptionsHandle(error: error)));
        } else {
          emit(AddFavoriteState(isLoading: false, message: error.toString()));
        }
      }
    });
  }
}
