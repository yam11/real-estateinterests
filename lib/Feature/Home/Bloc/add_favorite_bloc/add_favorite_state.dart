part of 'add_favorite_bloc.dart';

@immutable
class AddFavoriteState {
  bool? isLoading;
  String? message;
  AddFavoriteState({this.isLoading,this.message});
}
