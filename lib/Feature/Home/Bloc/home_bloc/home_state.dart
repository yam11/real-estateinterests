part of 'home_bloc.dart';

@immutable
abstract class HomeState {}

class HomeInitial extends HomeState {}

class SuccessToGetAllPosts extends HomeState {}

class ErrorToGetAllPosts extends HomeState {
  final String message;
  ErrorToGetAllPosts({required this.message});
}

class LoadingToGetAllPosts extends HomeState {}

class SuccessToAddComment extends HomeState {}

class ErrorToAddComment extends HomeState {
  final String message;
  ErrorToAddComment({required this.message});
}

class LoadingToAddComment extends HomeState {}

class LoadingToAddPostToFavorite extends HomeState {}

class SuccessToAddPostToFavorite extends HomeState {}

class ErrorToAddPostToFavorite extends HomeState {
  final String message;
  ErrorToAddPostToFavorite({required this.message});
}
