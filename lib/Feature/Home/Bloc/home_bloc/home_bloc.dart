import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:senior/Core/Api/ExceptionsHandle.dart';
import 'package:senior/Core/Api/Network.dart';
import 'package:senior/Core/Api/Urls.dart';
import 'package:senior/Feature/Home/Models/all_comments_model.dart';
import 'package:senior/Feature/Home/Models/all_posts_model.dart';
import 'package:senior/Feature/Home/Models/single_post_model.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  ListOfPostsModel? listOfPostsModel;
  ListOfCommentsModel? listOfCommentsModel;
  SinglePostModel? singlePostModel;
  int activeIndex = 0;

  ScrollController scrollController = ScrollController();
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  HomeBloc() : super(HomeInitial()) {
    on<HomeEvent>((event, emit) async {
      if (event is GetAllPosts) {
        emit(LoadingToGetAllPosts());
        try {
          await Network.getData(url: Urls.getAllPosts).then((value) {
            listOfPostsModel = ListOfPostsModel.fromJson(value.data);
            emit(SuccessToGetAllPosts());
          });
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetAllPosts(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetAllPosts(message: error.toString()));
          }
        }
      }
      if (event is GetAllComments) {
        if (event.loadingState) {
          emit(LoadingToGetAllPosts());
        }

        try {
          await Network.getData(
                  url: "${Urls.getAllCommentsByPostId}${event.postId}/comments")
              .then((value) {
            listOfCommentsModel = ListOfCommentsModel.fromJson(value.data);

            emit(SuccessToGetAllPosts());
          });
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetAllPosts(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetAllPosts(message: error.toString()));
          }
        }
      }
      if (event is AddNewComment) {
        emit(LoadingToAddComment());
        try {
          await Network.postData(
              url: "${Urls.getAllCommentsByPostId}${event.postId}/comments",
              data: {"rating": "5", "comment": event.comment}).then((value) {});
          emit(SuccessToAddComment());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToAddComment(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToAddComment(message: error.toString()));
          }
        }
      }

      if (event is GetSinglePost) {
        emit(LoadingToGetAllPosts());
        try {
          await Network.getData(url: "${Urls.getAllPosts}/${event.postId}")
              .then((value) {
            singlePostModel = SinglePostModel.fromJson(value.data);
          });
          emit(SuccessToGetAllPosts());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetAllPosts(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetAllPosts(message: error.toString()));
          }
        }
      }
      if (event is AddPostToFavoriteList) {
        emit(LoadingToAddPostToFavorite());
        try {
          final response = await Network.postData(
            url: "${Urls.favorites}/${event.postId}",
          );
          if (response.statusCode == 500) {
            emit(ErrorToAddPostToFavorite(message: "error"));
            return;
          }
          emit(SuccessToGetAllPosts());
        } catch (error) {
          if (error is DioError) {
            if (error.response!.statusCode == 500) {
              emit(ErrorToAddPostToFavorite(
                  message: exceptionsHandle(error: error)));
            }
            emit(ErrorToAddPostToFavorite(
                message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToAddPostToFavorite(message: error.toString()));
          }
        }
      }
    });
  }
}
