// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

class GetAllPosts extends HomeEvent {}

class GetAllComments extends HomeEvent {
  final String postId;
  final bool loadingState;
  GetAllComments({required this.loadingState, required this.postId});
}

class AddNewComment extends HomeEvent {
  final String postId;
  final String comment;

  AddNewComment({required this.comment, required this.postId});
}

class GetSinglePost extends HomeEvent {
  final String postId;
  GetSinglePost({required this.postId});
}

class AddPostToFavoriteList extends HomeEvent {
  final String postId;
  AddPostToFavoriteList({required this.postId});
}
