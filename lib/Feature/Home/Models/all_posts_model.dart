class ListOfPostsModel {
  List<RealEstate>? realEstate;

  ListOfPostsModel({this.realEstate});

  ListOfPostsModel.fromJson(Map<String, dynamic> json) {
    if (json['realEstate'] != null) {
      realEstate = <RealEstate>[];
      json['realEstate'].forEach((v) {
        realEstate!.add(new RealEstate.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.realEstate != null) {
      data['realEstate'] = this.realEstate!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class RealEstate {
  Coordinates? coordinates;
  List<String>? images;
  String? label;
  bool? featured;
  String? sId;
  String? location;
  String? description;
  String? category;
  int? price;
  Owner? owner;
  List<Comments>? comments;
  String? createdAt;
  String? updatedAt;
  int? iV;

  RealEstate(
      {this.coordinates,
      this.images,
      this.label,
      this.featured,
      this.sId,
      this.location,
      this.description,
      this.category,
      this.price,
      this.owner,
      this.comments,
      this.createdAt,
      this.updatedAt,
      this.iV});

  RealEstate.fromJson(Map<String, dynamic> json) {
    coordinates = json['coordinates'] != null
        ? new Coordinates.fromJson(json['coordinates'])
        : null;
    images = json['images'].cast<String>();
    label = json['label'];
    featured = json['featured'];
    sId = json['_id'];
    location = json['location'];
    description = json['description'];
    category = json['category'];
    price = json['price'];
    owner = json['owner'] != null ? new Owner.fromJson(json['owner']) : null;
    if (json['comments'] != null) {
      comments = <Comments>[];
      json['comments'].forEach((v) {
        comments!.add(new Comments.fromJson(v));
      });
    }
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.coordinates != null) {
      data['coordinates'] = this.coordinates!.toJson();
    }
    data['images'] = this.images;
    data['label'] = this.label;
    data['featured'] = this.featured;
    data['_id'] = this.sId;
    data['location'] = this.location;
    data['description'] = this.description;
    data['category'] = this.category;
    data['price'] = this.price;
    if (this.owner != null) {
      data['owner'] = this.owner!.toJson();
    }
    if (this.comments != null) {
      data['comments'] = this.comments!.map((v) => v.toJson()).toList();
    }
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class Coordinates {
  String? type;
  List<double>? coordinates;

  Coordinates({this.type, this.coordinates});

  Coordinates.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    coordinates = json['coordinates'].cast<double>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['coordinates'] = this.coordinates;
    return data;
  }
}

class Owner {
  String? firstName;
  String? lastName;
  bool? admin;
  String? mobileNumber;
  String? walletAddress;
  String? sId;
  String? username;
  int? iV;

  Owner(
      {this.firstName,
      this.lastName,
      this.admin,
      this.mobileNumber,
      this.walletAddress,
      this.sId,
      this.username,
      this.iV});

  Owner.fromJson(Map<String, dynamic> json) {
    firstName = json['firstName'];
    lastName = json['lastName'];
    admin = json['admin'];
    mobileNumber = json['mobileNumber'];
    walletAddress = json['walletAddress'];
    sId = json['_id'];
    username = json['username'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['admin'] = this.admin;
    data['mobileNumber'] = this.mobileNumber;
    data['walletAddress'] = this.walletAddress;
    data['_id'] = this.sId;
    data['username'] = this.username;
    data['__v'] = this.iV;
    return data;
  }
}

class Comments {
  String? sId;
  int? rating;
  String? comment;
  Owner? author;
  String? createdAt;
  String? updatedAt;

  Comments(
      {this.sId,
      this.rating,
      this.comment,
      this.author,
      this.createdAt,
      this.updatedAt});

  Comments.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    rating = json['rating'];
    comment = json['comment'];
    author = json['author'] != null
        ? new Owner.fromJson(json['author'])
        : json['user'] != null
            ? new Owner.fromJson(json['user'])
            : null;
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['rating'] = this.rating;
    data['comment'] = this.comment;
    if (this.author != null) {
      data['author'] = this.author!.toJson();
    } else {
      data['user'] = this.author!.toJson();
    }
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}
