class ListOfCommentsModel {
  List<Comments>? comments;

  ListOfCommentsModel({this.comments});

  ListOfCommentsModel.fromJson(Map<String, dynamic> json) {
    if (json['comments'] != null) {
      comments = <Comments>[];
      json['comments'].forEach((v) {
        comments!.add(new Comments.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.comments != null) {
      data['comments'] = this.comments!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Comments {
  String? sId;
  int? rating;
  String? comment;
  Author? author;
  String? createdAt;
  String? updatedAt;

  Comments(
      {this.sId,
      this.rating,
      this.comment,
      this.author,
      this.createdAt,
      this.updatedAt});

  Comments.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    rating = json['rating'];
    comment = json['comment'];
    author =
        json['author'] != null ? new Author.fromJson(json['author']) : null;
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['rating'] = this.rating;
    data['comment'] = this.comment;
    if (this.author != null) {
      data['author'] = this.author!.toJson();
    }
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}

class Author {
  String? firstName;
  String? lastName;
  bool? admin;
  String? mobileNumber;
  String? walletAddress;
  String? sId;
  String? username;
  int? iV;

  Author(
      {this.firstName,
      this.lastName,
      this.admin,
      this.mobileNumber,
      this.walletAddress,
      this.sId,
      this.username,
      this.iV});

  Author.fromJson(Map<String, dynamic> json) {
    firstName = json['firstName'];
    lastName = json['lastName'];
    admin = json['admin'];
    mobileNumber = json['mobileNumber'];
    walletAddress = json['walletAddress'];
    sId = json['_id'];
    username = json['username'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['admin'] = this.admin;
    data['mobileNumber'] = this.mobileNumber;
    data['walletAddress'] = this.walletAddress;
    data['_id'] = this.sId;
    data['username'] = this.username;
    data['__v'] = this.iV;
    return data;
  }
}
