import 'package:flutter/material.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Feature/Home/Models/all_posts_model.dart';
import 'package:sizer/sizer.dart';

class MoreDetailsWidget extends StatelessWidget {
  final String category;
  final String location;
  final int price;
  final Owner owner;
  const MoreDetailsWidget(
      {required this.price,
      required this.location,
      required this.category,
      required this.owner,
      super.key});

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
          dividerColor: Colors.transparent,
          unselectedWidgetColor:
              AppColors.getPrimarySeconedary(), // here for close state
          colorScheme:
              ColorScheme.light(primary: AppColors.getPrimarySeconedary())),
      child: ExpansionTile(
        title: Text(
          "More Info",
          style: TextStyle(
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
              color: AppColors.getPrimarySeconedary()),
        ),
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 5.w),
            child: Column(
              children: [
                SizedBox(
                  height: 2.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("About",
                        style: TextStyle(
                            color: AppColors.getPrimarySeconedary(),
                            fontSize: 20.sp,
                            fontWeight: FontWeight.bold)),
                  ],
                ),
                SizedBox(
                  height: 1.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Category :",
                        style: TextStyle(
                            color: AppColors.getPrimarySeconedary(),
                            fontSize: 15.sp,
                            fontWeight: FontWeight.bold)),
                    SizedBox(
                      width: 5.w,
                    ),
                    Expanded(
                        child: Text(category,
                            style: TextStyle(
                              color: AppColors.greyColor,
                              fontSize: 15.sp,
                            )))
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("location :",
                        style: TextStyle(
                            color: AppColors.getPrimarySeconedary(),
                            fontSize: 15.sp,
                            fontWeight: FontWeight.bold)),
                    SizedBox(
                      width: 5.w,
                    ),
                    Expanded(
                        child: Text(location,
                            style: TextStyle(
                              color: AppColors.greyColor,
                              fontSize: 15.sp,
                            )))
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Price :",
                        style: TextStyle(
                            color: AppColors.getPrimarySeconedary(),
                            fontSize: 15.sp,
                            fontWeight: FontWeight.bold)),
                    SizedBox(
                      width: 5.w,
                    ),
                    Expanded(
                        child: Text(price.toString(),
                            style: TextStyle(
                              color: AppColors.greyColor,
                              fontSize: 15.sp,
                            )))
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Owner",
                        style: TextStyle(
                            color: AppColors.getPrimarySeconedary(),
                            fontSize: 20.sp,
                            fontWeight: FontWeight.bold)),
                  ],
                ),
                SizedBox(
                  height: 1.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Name :",
                        style: TextStyle(
                            color: AppColors.getPrimarySeconedary(),
                            fontSize: 15.sp,
                            fontWeight: FontWeight.bold)),
                    SizedBox(
                      width: 5.w,
                    ),
                    Expanded(
                        child: Row(
                      children: [
                        Text("yaser",
                            style: TextStyle(
                              color: AppColors.greyColor,
                              fontSize: 15.sp,
                            )),
                        SizedBox(
                          width: 1.w,
                        ),
                        Text("tm",
                            style: TextStyle(
                              color: AppColors.greyColor,
                              fontSize: 15.sp,
                            )),
                      ],
                    ))
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Phone :",
                        style: TextStyle(
                            color: AppColors.getPrimarySeconedary(),
                            fontSize: 15.sp,
                            fontWeight: FontWeight.bold)),
                    SizedBox(
                      width: 5.w,
                    ),
                    Expanded(
                        child: Text("0987654321",
                            style: TextStyle(
                              color: AppColors.greyColor,
                              fontSize: 15.sp,
                            )))
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Email :",
                        style: TextStyle(
                            color: AppColors.getPrimarySeconedary(),
                            fontSize: 15.sp,
                            fontWeight: FontWeight.bold)),
                    SizedBox(
                      width: 5.w,
                    ),
                    Expanded(
                        child: Text("yaser@gmail.com",
                            style: TextStyle(
                              color: AppColors.greyColor,
                              fontSize: 15.sp,
                            )))
                  ],
                ),
                SizedBox(
                  height: 4.h,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
