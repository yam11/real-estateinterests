import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senior/App/app_localizations.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Widgets/pub_up_dialog.dart';
import 'package:senior/Feature/Home/Bloc/remove_post_bloc/remove_post_bloc.dart';
import 'package:senior/Feature/Home/Models/all_posts_model.dart';

class PostOptionsWidget extends StatelessWidget {
  int postIndex;
  List<RealEstate> realEstate;
  RemovePostBloc removePostBloc;

  PostOptionsWidget(
      {required this.removePostBloc,
      required this.realEstate,
      required this.postIndex,
      Key? key})
      : super(key: key);
  Map<String, String> options = {
    "Edit Post": "Edit Post",
    "Delete Post": "Delete Post"
  };
  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.end, children: [
      PopupMenuButton<String>(
          icon: Icon(
            Icons.menu,
            color: AppColors.getWhiteBlack(),
          ),
          color: AppColors.white2Color,
          onSelected: (value) {
            if (value == "Edit Post") {}
            if (value == "Delete Post") {
              showYesNoDialog(
                  context, "Are you sure you want to delete post ?".tr(context),
                  () {
                removePostBloc.add(RemovePost(
                    postId: realEstate[postIndex].sId!,
                    index: postIndex,
                    realEstate: realEstate));
                Navigator.of(context).pop();
              }, () {
                Navigator.of(context).pop();
              });
            }
          },
          itemBuilder: (context) {
            return [
              PopupMenuItem(
                value: options["Edit Post"],
                child: Text(options["Edit Post"]!.tr(context)),
              ),
              PopupMenuItem(
                value: options["Delete Post"],
                child: Text(options["Delete Post"]!.tr(context)),
              ),
            ];
          })
    ]);
  }
}
