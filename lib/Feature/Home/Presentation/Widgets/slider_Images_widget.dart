import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Widgets/cached_network_image.dart';
import 'package:senior/Feature/Home/Presentation/Widgets/view_post_images.dart';
import 'package:sizer/sizer.dart';
import 'package:shimmer/shimmer.dart';

class SliderImagesWidget extends StatelessWidget {
  final List<String> images;
  const SliderImagesWidget({required this.images, super.key});

  @override
  Widget build(BuildContext context) {
    if (images.isNotEmpty) {
      if (images.length == 1) {
        return oneImage(context, images);
      }
      if (images.length == 2) {
        return twoImages(context, images);
      } else {
        return numberOfImages(context, images);
      }
    } else {
      return const SizedBox();
    }
  }
}

Widget oneImage(BuildContext context, List<String> images) {
  return InkWell(
    onTap: () {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (_) => ViewPostImagesWidget(
                items: images.length,
                image: images,
                activeIndex: 0,
              )));
    },
    child: Container(
      height: 40.h,
      // decoration: BoxDecoration(
      //     image: DecorationImage(
      //         image: NetworkImage(images[0]), fit: BoxFit.cover)),
      child: MyCachedNetwork(imageUrl: images[0]),
    ),
  );
}

Widget twoImages(BuildContext context, List<String> images) {
  return SizedBox(
    height: 40.h,
    child: Row(
      children: [
        Expanded(
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => ViewPostImagesWidget(
                        items: images.length,
                        image: images,
                        activeIndex: 0,
                      )));
            },
            child: Container(
              height: 40.h,
              // decoration: BoxDecoration(
              //     image: DecorationImage(
              //         image: NetworkImage(images[0]), fit: BoxFit.cover)),
              child: MyCachedNetwork(imageUrl: images[0]),
            ),
          ),
        ),
        SizedBox(
          width: 1.w,
        ),
        Expanded(
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => ViewPostImagesWidget(
                        items: images.length,
                        image: images,
                        activeIndex: 1,
                      )));
            },
            child: Container(
              height: 40.h,
              // decoration: BoxDecoration(
              //     image: DecorationImage(
              //         image: NetworkImage(images[0]), fit: BoxFit.cover)),
              child: MyCachedNetwork(imageUrl: images[1]),
            ),
          ),
        ),
      ],
    ),
  );
}

Widget numberOfImages(BuildContext context, List<String> images) {
  return SizedBox(
    height: 40.h,
    child: Row(
      children: [
        Expanded(
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => ViewPostImagesWidget(
                        items: images.length,
                        image: images,
                        activeIndex: 0,
                      )));
            },
            child: Container(
      height: 40.h,
      // decoration: BoxDecoration(
      //     image: DecorationImage(
      //         image: NetworkImage(images[0]), fit: BoxFit.cover)),
      child: MyCachedNetwork(imageUrl: images[0]),
    ),
          ),
        ),
        SizedBox(
          width: 1.w,
        ),
        Expanded(
          child: Column(
            children: [
              Expanded(
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => ViewPostImagesWidget(
                              items: images.length,
                              image: images,
                              activeIndex: 1,
                            )));
                  },
                  child: Container(
      height: 40.h,
      // decoration: BoxDecoration(
      //     image: DecorationImage(
      //         image: NetworkImage(images[0]), fit: BoxFit.cover)),
      child: MyCachedNetwork(imageUrl: images[1]),
    ),
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              Expanded(
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => ViewPostImagesWidget(
                              items: images.length,
                              image: images,
                              activeIndex: 2,
                            )));
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
      height: 40.h,
      // decoration: BoxDecoration(
      //     image: DecorationImage(
      //         image: NetworkImage(images[0]), fit: BoxFit.cover)),
      child: MyCachedNetwork(imageUrl: images[2]),
    ),
                      images.length > 3
                          ? Container(
                              height: 100.h,
                              width: double.infinity,
                              alignment: Alignment.center,
                              color: AppColors.blackColor.withOpacity(0.5),
                              child: Text(
                                "+${images.length - 3}",
                                style: TextStyle(
                                    color: AppColors.white2Color,
                                    fontSize: 25.sp),
                              ),
                            )
                          : const SizedBox()
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}
