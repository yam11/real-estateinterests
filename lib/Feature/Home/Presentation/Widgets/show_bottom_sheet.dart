import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:senior/Core/Constants/app_assets.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Widgets/comments_widget.dart';
import 'package:senior/Core/Widgets/dialog_loading.dart';
import 'package:senior/Core/Widgets/coustom_text_falid.dart';
import 'package:senior/Core/Widgets/loading_widget.dart';
import 'package:senior/Feature/Home/Bloc/home_bloc/home_bloc.dart';
import 'package:sizer/sizer.dart';

commentBottomSheet(
  BuildContext context,
  String postId,
) {
  HomeBloc homeBloc = HomeBloc();
  TextEditingController commentController = TextEditingController();
  FocusNode commentNode = FocusNode();

  return showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      barrierColor: Colors.transparent,
      context: context,
      builder: (context) {
        return BlocProvider(
          create: (context) =>
              homeBloc..add(GetAllComments(loadingState: true, postId: postId)),
          child: Container(
              height: 80.h,
              decoration: BoxDecoration(
                  color: AppColors.getBlackLightWhite(),
                  // image: const DecorationImage(
                  //   image: AssetImage(AppAssets.homeBackGraound),
                  // ),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5.h),
                      topRight: Radius.circular(5.h))),
              child: Column(
                children: [
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 15.w, vertical: 1.h),
                    child: const Divider(
                      thickness: 2,
                      color: AppColors.seconedaryColor,
                    ),
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 5.w),
                          child: CustomTextField(
                            isPassword: false,
                            iconColor: AppColors.getPrimarySeconedary(),
                            fillColor: AppColors.getBlackWhite(),
                            // prefixIconImage: AppAssets.lockIcon,
                            hintText: "Write Your Commet here",
                            focusNode: commentNode,
                            // textInputAction: TextInputAction.next,
                            textInputType: TextInputType.text,
                            controller: commentController,
                          ),
                        ),
                      ),
                      InkWell(
                          onTap: () {
                            if (commentController.text.isNotEmpty) {
                              homeBloc.add(AddNewComment(
                                  comment: commentController.text,
                                  postId: postId));
                            } else {
                              Fluttertoast.showToast(
                                  msg: "Comment field is empty",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 3,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            }
                          },
                          child: Container(
                            height: 5.h,
                            width: 10.w,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: AppColors.getPrimarySeconedary(),
                                shape: BoxShape.circle),
                            child: Icon(
                              Icons.send,
                              color: AppColors.getBlackLightWhite(),
                            ),
                          )),
                      SizedBox(
                        width: 2.w,
                      )
                    ],
                  ),
                  Expanded(
                      child: BlocConsumer<HomeBloc, HomeState>(
                    listener: (context, state) {
                      if (state is LoadingToAddComment) {
                        showLoaderDialog(context);
                      }
                      if (state is ErrorToAddComment) {
                        Navigator.of(context).pop();
                        Fluttertoast.showToast(
                            msg: state.message,
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 3,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            fontSize: 16.0);
                      }
                      if (state is SuccessToAddComment) {
                        Navigator.of(context).pop();

                        homeBloc.add(GetAllComments(
                            loadingState: false, postId: postId));
                        Fluttertoast.showToast(
                            msg: "Success to add Comment",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 3,
                            backgroundColor: Colors.green,
                            textColor: Colors.white,
                            fontSize: 16.0);
                        homeBloc.scrollController.animateTo(
                            homeBloc.scrollController.position.maxScrollExtent +
                                250,
                            curve: Curves.linear,
                            duration: const Duration(seconds: 2));
                        commentController.clear();
                      }
                    },
                    builder: (context, state) {
                      if (state is SuccessToGetAllPosts ||
                          state is SuccessToAddComment ||
                          state is ErrorToAddComment ||
                          state is LoadingToAddComment) {
                        return homeBloc
                                .listOfCommentsModel!.comments!.isNotEmpty
                            ? ListView.builder(
                                controller: homeBloc.scrollController,
                                physics: const BouncingScrollPhysics(
                                    parent: AlwaysScrollableScrollPhysics()),
                                itemCount: homeBloc
                                    .listOfCommentsModel!.comments!.length,
                                itemBuilder: (context, index) {
                                  return CommentsWidget(
                                    avatarImage: AppAssets.emailIcon,
                                    index: index,
                                    comment: homeBloc.listOfCommentsModel!
                                        .comments![index].comment!,
                                    firstName: homeBloc.listOfCommentsModel!
                                        .comments![index].author!.firstName!,
                                    lastName: homeBloc.listOfCommentsModel!
                                        .comments![index].author!.lastName!,
                                  );
                                },
                              )
                            : Center(
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 2.w, vertical: 1.h),
                                  decoration: BoxDecoration(
                                      color: AppColors.whiteColor,
                                      borderRadius: BorderRadius.circular(5.w)),
                                  child: Text("No Comments"),
                                ),
                              );
                      } else {
                        return  Center(
                          child: LoadingWidget(),
                        );
                      }
                    },
                  ))
                ],
              )),
        );
      });
}
