import 'package:flutter/material.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class ViewPostImagesWidget extends StatelessWidget {
  int items;
  List image;
  int activeIndex;

  ViewPostImagesWidget(
      {Key? key,
      required this.items,
      required this.activeIndex,
      required this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    PageController pageController = PageController(initialPage: activeIndex);
    return Scaffold(
      body: Stack(
        children: [
          PhotoViewGallery.builder(
            itemCount: items,
            pageController: pageController,
            builder: (context, index) {
              return PhotoViewGalleryPageOptions(
                  imageProvider: NetworkImage(image[index]));
            },
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 4.h,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 2.w,
                  ),
                  IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: Icon(
                        Icons.arrow_back,
                        size: 25.sp,
                        color: AppColors.primaryColor,
                      )),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
