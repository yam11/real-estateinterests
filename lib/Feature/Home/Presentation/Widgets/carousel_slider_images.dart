import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Feature/Home/Models/all_posts_model.dart';
import 'package:senior/Feature/Home/Presentation/Widgets/view_post_images.dart';
import 'package:sizer/sizer.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class CarouselSliderImages extends StatefulWidget {
  List<String> images;
  final Owner owner;
  CarouselSliderImages({required this.owner, required this.images, super.key});

  @override
  State<CarouselSliderImages> createState() => _CarouselSliderImagesState();
}

class _CarouselSliderImagesState extends State<CarouselSliderImages> {
  int activeIndex = 0;

  CarouselController carouselController = CarouselController();

  nextPage() {
    carouselController.nextPage();
  }

  previousPage() {
    carouselController.previousPage();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      height: 50.h,
                      decoration: const BoxDecoration(
                          // border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                      child: CarouselSlider.builder(
                          carouselController: carouselController,
                          itemCount: widget.images.length,
                          itemBuilder: (context, index, realIndex) {
                            return InkWell(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (_) => ViewPostImagesWidget(
                                          items: widget.images.length,
                                          image: widget.images,
                                          activeIndex: index,
                                        )));
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            width: 1,
                                            color: AppColors.getWhiteBlack())),
                                    image: DecorationImage(
                                        image:
                                            NetworkImage(widget.images[index]),
                                        fit: BoxFit.contain)),
                              ),
                            );
                          },
                          options: CarouselOptions(
                            scrollPhysics: const NeverScrollableScrollPhysics(),
                            enableInfiniteScroll: false,
                            height: 50.h,
                            autoPlay: true,
                            viewportFraction: 1,
                            onPageChanged: (index, reason) {
                              return setState(() => activeIndex = index);
                            },
                          )),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                            onPressed: () {
                              previousPage();
                            },
                            icon: Icon(
                              Icons.arrow_back_outlined,
                              color: AppColors.getPrimarySeconedary(),
                              size: 10.w,
                            )),
                        IconButton(
                            onPressed: () {
                              nextPage();
                            },
                            icon: Icon(
                              Icons.arrow_forward_outlined,
                              color: AppColors.getPrimarySeconedary(),
                              size: 10.w,
                            )),
                      ],
                    )
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 1.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 4.w, vertical: 1.h),
                        decoration: BoxDecoration(
                            color: AppColors.getBlackGrey(),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(5.w),
                                bottomRight: Radius.circular(5.w))),
                        child: AnimatedSmoothIndicator(
                            activeIndex: activeIndex,
                            count: widget.images.length,
                            effect: WormEffect(
                                dotHeight: 10,
                                dotWidth: 10,
                                dotColor: AppColors.getGreyWhite(),
                                activeDotColor: AppColors.primaryColor)),
                      )
                    ],
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(left: 4.w, top: 45.h),
              child: Row(
                children: [
                  CircleAvatar(
                      // backgroundImage: NetworkImage(widget.avatarImage),
                      radius: 10.w,
                      backgroundColor: AppColors.greyColor,
                      // foregroundColor: Colors.brown.withOpacity(0.5),
                      child: Icon(
                        Icons.person,
                        size: 25.sp,
                        color: AppColors.white2Color,
                      )),
                  const SizedBox(
                    width: 5,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 7.h),
                    child: Column(
                      children: [
                        Text(
                          widget.owner.firstName!,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16.sp,
                              color: Colors.grey,
                              fontWeight: FontWeight.bold),
                        ),
                           Text(
                          widget.owner.lastName!,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16.sp,
                              color: Colors.grey,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ],
    );
  }
}
