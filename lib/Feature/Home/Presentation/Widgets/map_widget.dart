import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Feature/Home/Models/single_post_model.dart';
import 'package:sizer/sizer.dart';
import 'package:map_launcher/map_launcher.dart' as maplauncher;
import 'package:flutter/foundation.dart';

class MapWidget extends StatefulWidget {
  final Coordinates coordinates;
  final String lat;
  final String lang;
  const MapWidget(
      {required this.lang,
      required this.lat,
      required this.coordinates,
      super.key});

  @override
  State<MapWidget> createState() => _MapWidgetState();
}

class _MapWidgetState extends State<MapWidget> {
  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> myMarker = {};
  late CameraPosition initialSettings;

  @override
  void initState() {
    initialSettings = CameraPosition(
      target: LatLng(widget.coordinates.coordinates![0],
          widget.coordinates.coordinates![1]),
      zoom: 16.0,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30.h,
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 2.w, vertical: 2.h),
      decoration: BoxDecoration(
          border:
              Border.all(width: 2, color: AppColors.getPrimarySeconedary())),
      child: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
        return GoogleMap(
          gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>{
            Factory<OneSequenceGestureRecognizer>(
              () => EagerGestureRecognizer(),
            ),
          },
          mapType: MapType.hybrid,
          initialCameraPosition: initialSettings,
          myLocationEnabled: true,
          onMapCreated: (GoogleMapController controller) {
            setState(() {
              myMarker.add(Marker(
                  consumeTapEvents: true,
                  onTap: () async {
                    try {
                      if ((await maplauncher.MapLauncher.isMapAvailable(
                          maplauncher.MapType.google))!) {
                        await maplauncher.MapLauncher.showMarker(
                          mapType: maplauncher.MapType.google,
                          coords: maplauncher.Coords(double.parse(widget.lat),
                              double.parse(widget.lang)),
                          title: 'trip location',
                        );
                      }
                      print('hello world map 2');
                    } catch (error) {
                      print('hello world map 2 error $error');
                    }
                  },
                  markerId: MarkerId("1"),
                  position: LatLng(widget.coordinates.coordinates![0],
                      widget.coordinates.coordinates![1])));
            });
            _controller.complete(controller);
          },
          markers: myMarker,
        );
      }),
    );
  }
}
