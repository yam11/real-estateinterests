import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../../../Core/Constants/app_colors.dart';

dialogConf(BuildContext context, String title) {
  return showCupertinoModalPopup(
      context: context,
      builder: (_) {
        return Center(
          child: Material(
            borderRadius: BorderRadius.circular(6.h),
            child: Container(
              height: 25.h,
              width: 45.h,
              padding: EdgeInsets.symmetric(
                horizontal: 1.h,
                vertical: 1.5,
              ),
              decoration: BoxDecoration(
                color: AppColors.getBlackWhite(),
                borderRadius: BorderRadius.circular(6.h),
                border: Border.all(
                  color: Colors.grey,
                  width: 1.5,
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Text(
                          title,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: AppColors.getWhiteBlack(),
                            fontSize: 15.sp,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 4.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 4.5.h,
                        width: 14.h,
                        child: MaterialButton(
                          color: Colors.green,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(1.5.h),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            "OK",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 10.sp,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 4.h,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      });
}
