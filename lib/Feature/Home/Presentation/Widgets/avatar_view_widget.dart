import 'package:flutter/material.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class AvatarViewWidget extends StatelessWidget {
  final String image;

  const AvatarViewWidget({Key? key, required this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.topLeft,
        children: [
          PhotoViewGallery(
            pageOptions: [  
              PhotoViewGalleryPageOptions(imageProvider: AssetImage(image))
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 4.h,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 2.w,
                  ),
                  IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: Icon(
                        Icons.arrow_back,
                        size: 25.sp,
                        color: AppColors.primaryColor,
                      )),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
