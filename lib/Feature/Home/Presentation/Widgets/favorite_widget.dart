import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senior/App/app_localizations.dart';
import 'package:senior/Core/Widgets/dialog_loading.dart';
import 'package:senior/Feature/Home/Bloc/add_favorite_bloc/add_favorite_bloc.dart';
import 'package:senior/Feature/Home/Bloc/home_bloc/home_bloc.dart';

class FavoriteWidget extends StatelessWidget {
  String postId;
  bool isAdd;

  FavoriteWidget({required this.isAdd, required this.postId, super.key});
  bool enable = false;
  AddFavoriteBloc favoriteBloc = AddFavoriteBloc();
  @override
  Widget build(BuildContext context) {
    print("ssssssssssssssssssssss$postId");
    return BlocProvider(
      create: (context) => favoriteBloc,
      child: BlocListener<AddFavoriteBloc, AddFavoriteState>(
        listener: (context, state) {
          if (state.isLoading! && state.message == null) {
            showLoaderDialog(context);
          }
          if (!state.isLoading! && state.message != null) {
            Navigator.of(context).pop();
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              backgroundColor: Colors.red,
              duration: Duration(seconds: 3),
              content: Text(
                isAdd
                    ? "Already in favorite list".tr(context)
                    : "Already remove from favorite list".tr(context),
                textAlign: TextAlign.center,
              ),
            ));
          }
          if (!state.isLoading! && state.message == null) {
            Navigator.of(context).pop();

            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              backgroundColor: Colors.green,
              duration: Duration(seconds: 3),
              content: Text(
                isAdd
                    ? "Success add to favorite list".tr(context)
                    : "Success remove from favorite list".tr(context),
                textAlign: TextAlign.center,
              ),
            ));
          }
        },
        child: IconButton(
            onPressed: () {
              if (isAdd) {
                favoriteBloc.add(AddFavorite(postId: postId));
              } else {
                favoriteBloc.add(RemoveFavorite(postId: postId));
              }
            },
            icon: const Icon(
              Icons.favorite,
              color: Colors.red,
            )),
      ),
    );
  }
}
