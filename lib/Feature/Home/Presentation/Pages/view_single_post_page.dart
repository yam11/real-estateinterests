import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:senior/App/Bloc/scroll_bloc/scroll_bloc.dart';
import 'package:senior/App/app_localizations.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Widgets/app_bar_widget.dart';
import 'package:senior/Core/Widgets/error_message_widget.dart';
import 'package:senior/Core/Widgets/loading_widget.dart';
import 'package:senior/Core/Widgets/scroll_widget.dart';
import 'package:senior/Feature/Home/Bloc/home_bloc/home_bloc.dart';
import 'package:senior/Feature/Home/Models/all_posts_model.dart';
import 'package:senior/Feature/Home/Presentation/Pages/wallet_page.dart';
import 'package:senior/Feature/Home/Presentation/Widgets/carousel_slider_images.dart';
import 'package:senior/Feature/Home/Presentation/Widgets/dilog_conf.dart';
import 'package:senior/Feature/Home/Presentation/Widgets/favorite_widget.dart';
import 'package:senior/Feature/Home/Presentation/Widgets/map_widget.dart';
import 'package:senior/Feature/Home/Presentation/Widgets/more_details_widget.dart';
import 'package:senior/Feature/Home/Presentation/Widgets/show_bottom_sheet.dart';
import 'package:sizer/sizer.dart';
import 'dart:ui' as ui;

class ViewSinglePostPage extends StatelessWidget {
  final String postId;
  final Owner owner;

  ViewSinglePostPage({required this.owner, required this.postId, Key? key})
      : super(key: key);

  bool isArabic(String text) {
    RegExp arabicRegex = RegExp(
        r'[\u0600-\u06FF\u0750-\u077F\u08A0-\u08FF\uFB50-\uFDFD\uFE70-\uFEFF]+');
    return arabicRegex.hasMatch(text);
  }

  HomeBloc homeBloc = HomeBloc();
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => homeBloc..add(GetSinglePost(postId: postId)),
      child: Scaffold(
        backgroundColor: AppColors.getBlackLightWhite(),
        extendBody: true,
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(AppBar().preferredSize.height),
          child: BlocBuilder<ScrollBloc, ScrollState>(
            builder: (context, state) {
              return SafeArea(
                child: AnimatedContainer(
                    height: context.read<ScrollBloc>().appBar
                        ? AppBar().preferredSize.height
                        : Size.zero.height,
                    duration: const Duration(milliseconds: 400),
                    child: AppBarWidget(
                      buttonBack: true,
                      title: "Post Details".tr(context),
                    )),
              );
            },
          ),
        ),
        body: SafeArea(
          child: ScrollWidget(
            child: BlocProvider(
              create: (context) => homeBloc..add(GetSinglePost(postId: postId)),
              child: BlocConsumer<HomeBloc, HomeState>(
                listener: (context, state) {},
                builder: (context, state) {
                  if (state is ErrorToGetAllPosts) {
                    return ErrorMessageWidget(
                        onPressed: () {
                          homeBloc.add(GetSinglePost(postId: postId));
                        },
                        message: state.message);
                  }
                  if (state is SuccessToGetAllPosts) {
                    return SmartRefresher(
                      physics: const BouncingScrollPhysics(
                          parent: AlwaysScrollableScrollPhysics()),
                      enablePullDown: true,
                      controller: refreshController,
                      onRefresh: () {
                        homeBloc.add(GetSinglePost(postId: postId));
                      },
                      child: ListView(
                        physics: const NeverScrollableScrollPhysics(),
                        children: [
                          homeBloc.singlePostModel!.realEstate!.images!
                                  .isNotEmpty
                              ? CarouselSliderImages(
                                  owner: owner,
                                  images: homeBloc
                                      .singlePostModel!.realEstate!.images!)
                              : Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 4.w, vertical: 2.h),
                                  child: Row(
                                    children: [
                                      CircleAvatar(
                                          // backgroundImage: NetworkImage(widget.avatarImage),
                                          radius: 10.w,
                                          backgroundColor: AppColors.greyColor,
                                          // foregroundColor: Colors.brown.withOpacity(0.5),
                                          child: Icon(
                                            Icons.person,
                                            size: 25.sp,
                                            color: AppColors.white2Color,
                                          )),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 5.h),
                                        child: Column(
                                          children: [
                                            Text(
                                              owner.firstName!,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontSize: 16.sp,
                                                  color: Colors.grey,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              owner.lastName!,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontSize: 16.sp,
                                                  color: Colors.grey,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                          SizedBox(
                            height: 2.h,
                          ),
                          MoreDetailsWidget(
                            owner: owner,
                            category:
                                homeBloc.singlePostModel!.realEstate!.category!,
                            location:
                                homeBloc.singlePostModel!.realEstate!.location!,
                            price: 100,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              InkWell(
                                onTap: () {
                                  commentBottomSheet(context, postId);
                                },
                                child: Container(
                                    height: 4.h,
                                    width: 22.w,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: AppColors.primaryColor
                                            .withOpacity(0.25),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: Text(
                                      "Comments".tr(context),
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 10.sp,
                                          fontWeight: FontWeight.bold),
                                    )),
                              ),
                              FavoriteWidget(
                                isAdd: true,
                                postId:
                                    homeBloc.singlePostModel!.realEstate!.sId!,
                              )
                            ],
                          ),
                          SizedBox(
                            height: 2.h,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 1.w),
                            child: Directionality(
                              textDirection: isArabic(homeBloc.singlePostModel!
                                      .realEstate!.description!)
                                  ? ui.TextDirection.rtl
                                  : ui.TextDirection.ltr,
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      homeBloc.singlePostModel!.realEstate!
                                          .description!,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          color: AppColors.textColor(),
                                          fontSize: 14.sp),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: 2.h),
                          MapWidget(
                              lat: " 33.511749",
                              lang: "36.310276",
                              coordinates: homeBloc
                                  .singlePostModel!.realEstate!.coordinates!),
                          SizedBox(height: 5.h),
                          // Padding(
                          //   padding: EdgeInsets.symmetric(horizontal: 2.w),
                          //   child: MaterialButton(
                          //       color: AppColors.getPrimarySeconedary(),
                          //       shape: RoundedRectangleBorder(
                          //           side: BorderSide(
                          //               width: 1,
                          //               color:
                          //                   AppColors.getPrimarySeconedary())),
                          //       onPressed: () {
                          //         //  dialogConf(context,
                          //         //   "Your are not connecting to the wallet");
                          //         showWalletBottomSheet(context, price: 100);
                          //       },
                          //       child: Text(
                          //         "Buy".tr(context),
                          //         style: TextStyle(
                          //             color: AppColors.getBlackWhite(),
                          //             fontSize: 16.sp,
                          //             fontWeight: FontWeight.bold),
                          //       )),
                          // ),
                          SizedBox(height: 5.h),
                        ],
                      ),
                    );
                  } else {
                    return Center(
                      child: LoadingWidget(),
                    );
                  }
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
