import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:senior/App/app_localizations.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Widgets/dialog_loading.dart';

import 'package:senior/Core/Widgets/error_message_widget.dart';
import 'package:senior/Core/Widgets/loading_widget.dart';
import 'package:senior/Core/Widgets/scroll_widget.dart';
import 'package:senior/Feature/Home/Bloc/home_bloc/home_bloc.dart';
import 'package:senior/Feature/Home/Bloc/remove_post_bloc/remove_post_bloc.dart';
import 'package:senior/Feature/Home/Presentation/Widgets/post_widget.dart';

class HomePage extends StatelessWidget {
  HomePage({required this.homeBloc, super.key});

  HomeBloc homeBloc;
  RemovePostBloc removePostBloc = RemovePostBloc();
  @override
  Widget build(BuildContext context) {
    return ScrollWidget(
      child: BlocConsumer<HomeBloc, HomeState>(
        listener: (context, state) {},
        builder: (context, state) {
          if (state is ErrorToGetAllPosts) {
            return ErrorMessageWidget(
              message: state.message,
              onPressed: () {
                homeBloc.add(GetAllPosts());
              },
            );
          }
          if (state is SuccessToGetAllPosts ||
              state is LoadingToAddPostToFavorite ||
              state is ErrorToAddPostToFavorite ||
              state is SuccessToAddPostToFavorite) {
            return BlocProvider(
              create: (context) => removePostBloc,
              child: BlocConsumer<RemovePostBloc, RemovePostState>(
                listener: (context, state) {
                  if (state is PostRemoveState) {
                    if (state.isLoading) {
                      showLoaderDialog(context);
                    } else {
                      Navigator.of(context).pop();

                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        backgroundColor: Colors.green,
                        duration: Duration(seconds: 3),
                        content: Text(
                          "Success remove post".tr(context),
                          textAlign: TextAlign.center,
                        ),
                      ));
                    }
                  }
                  if (state is ErrorState) {
                    Navigator.of(context).pop();
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      backgroundColor: Colors.red,
                      duration: Duration(seconds: 3),
                      content: Text(
                        state.message,
                        textAlign: TextAlign.center,
                      ),
                    ));
                  }
                },
                builder: (context, state) {
                  return SmartRefresher(
                    controller: homeBloc.refreshController,
                    onRefresh: () async {
                      homeBloc.add(GetAllPosts());
                    },
                    physics: const BouncingScrollPhysics(
                        parent: AlwaysScrollableScrollPhysics()),
                    child: ListView.builder(
                      shrinkWrap: true,
                      physics: const BouncingScrollPhysics(
                          parent: AlwaysScrollableScrollPhysics()),
                      itemCount: homeBloc.listOfPostsModel!.realEstate!.length,
                      itemBuilder: (context, index) => PostWidget(
                          removePostBloc: removePostBloc,
                          listOfPostsModel: homeBloc.listOfPostsModel!,
                          index: index),
                    ),
                  );
                },
              ),
            );
          } else {
            return Center(child: LoadingWidget());
          }
        },
      ),
    );
  }
}
