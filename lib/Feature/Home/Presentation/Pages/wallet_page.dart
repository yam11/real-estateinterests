import 'package:flutter/material.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

showWalletBottomSheet(BuildContext context, {required int price}) {
  return showModalBottomSheet(
    barrierColor: Colors.black.withOpacity(0.7),
    backgroundColor: Colors.transparent,
    context: context,
    builder: (BuildContext context) {
      return Container(
        decoration: BoxDecoration(
            color: AppColors.whiteColor,
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(5.w), topLeft: Radius.circular(5.w))),
        height: 70.h,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
              height: 5.h,
            ),
            Expanded(
                child: ListView(
              physics: const BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics()),
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.w),
                  child: Row(
                    children: [
                      Text('Confirm',
                          style: TextStyle(
                              color: AppColors.blackColor,
                              fontSize: 18.sp,
                              fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.w),
                  child: Row(
                    children: [
                      Text('Please review your transaction',
                          style: TextStyle(
                              color: AppColors.greyColor,
                              fontSize: 14.sp,
                              fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
                Divider(
                  thickness: 1,
                  color: AppColors.greyColor,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 2.w),
                  child: Row(
                    children: [
                      Expanded(
                          child: Row(
                        children: [
                          CircleAvatar(
                            backgroundColor: AppColors.seconedaryColor,
                            radius: 5.w,
                          ),
                          Expanded(
                              child: Text('Account 1',
                                  style: TextStyle(
                                    color: AppColors.greyColor,
                                    fontSize: 14.sp,
                                  )))
                        ],
                      )),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 2.w),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Container(
                              width: 1,
                              height: 5.h,
                              color: AppColors.greyColor,
                            ),
                            CircleAvatar(
                              backgroundColor: AppColors.white2Color,
                              radius: 4.w,
                              child: Icon(
                                Icons.arrow_forward,
                                color: AppColors.greyColor,
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                          child: Row(
                        children: [
                          CircleAvatar(
                            backgroundColor: AppColors.primaryColor,
                            radius: 4.w,
                          ),
                          Expanded(
                              child: Text('Real Estate',
                                  style: TextStyle(
                                    color: AppColors.greyColor,
                                    fontSize: 14.sp,
                                  )))
                        ],
                      ))
                    ],
                  ),
                ),
                Divider(
                  thickness: 1,
                  color: AppColors.greyColor,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Text("\$$price",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: AppColors.greyColor,
                              fontSize: 28.sp,
                              fontWeight: FontWeight.bold)),
                    )
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: Text("USD",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: AppColors.greyColor,
                              fontSize: 28.sp,
                              fontWeight: FontWeight.bold)),
                    )
                  ],
                ),
                Divider(
                  thickness: 1,
                  color: AppColors.greyColor,
                ),
                SizedBox(
                  height: 3.h,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Text("From",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: AppColors.greyColor,
                              fontSize: 14.sp,
                              fontWeight: FontWeight.bold)),
                    ),
                    Expanded(
                      child: Text("Account 1",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: AppColors.greyColor,
                              fontSize: 14.sp,
                              fontWeight: FontWeight.bold)),
                    )
                  ],
                ),
                SizedBox(
                  height: 3.h,
                ),
                Divider(
                  thickness: 1,
                  color: AppColors.greyColor,
                ),
                SizedBox(
                  height: 3.h,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Text("To",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: AppColors.greyColor,
                              fontSize: 14.sp,
                              fontWeight: FontWeight.bold)),
                    ),
                    Expanded(
                      child: Text("Real Estate",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: AppColors.greyColor,
                              fontSize: 14.sp,
                              fontWeight: FontWeight.bold)),
                    )
                  ],
                ),
                SizedBox(
                  height: 3.h,
                ),
                Divider(
                  thickness: 1,
                  color: AppColors.greyColor,
                ),
                SizedBox(
                  height: 2.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MaterialButton(
                      height: 8.h,
                      onPressed: () {},
                      shape: RoundedRectangleBorder(
                          side:
                              BorderSide(width: 1, color: AppColors.greyColor)),
                      child: Text("CANCEL",
                          style: TextStyle(
                              color: AppColors.greyColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 16.sp)),
                    ),
                    SizedBox(
                      width: 3.w,
                    ),
                    MaterialButton(
                      height: 8.h,
                      color: Color.fromARGB(255, 68, 255, 183),
                      shape: RoundedRectangleBorder(
                          side:
                              BorderSide(width: 1, color: AppColors.greyColor)),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text("CONFIRM",
                          style: TextStyle(
                              color: AppColors.greyColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 16.sp)),
                    ),
                  ],
                ),
                SizedBox(
                  height: 5.h,
                ),
              ],
            ))
          ],
        ),
      );
    },
  );
}
