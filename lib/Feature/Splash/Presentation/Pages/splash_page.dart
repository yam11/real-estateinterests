import 'dart:async';

import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:senior/Core/Constants/app_assets.dart';

import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:senior/Feature/Auth/Presentation/Widgets/log_in_widget.dart';
import 'package:senior/Feature/Auth/Presentation/Pages/register_page.dart';

import 'package:senior/Feature/Main/Presentation/Pages/main_page.dart';
import 'package:senior/Feature/Splash/Presentation/Widgets/splash_painter.dart';

import 'package:sizer/sizer.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({super.key});

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    Timer(const Duration(milliseconds: 400), () {
      Navigator.pushReplacement(
          context,
          PageTransition(
              child:
                  AppSharedPreferences.hasToken ? MainPage() : RegisterPage(),
              type: PageTransitionType.rightToLeft,
              duration: const Duration(milliseconds: 400)));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      clipBehavior: Clip.none,
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: AppColors.seconedaryColor,
          child: CustomPaint(
            painter: SplashPainter(),
          ),
        ),
        Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset(
                AppAssets.splashImage,
                height: 250.0,
                fit: BoxFit.scaleDown,
                width: 250.0,
              ),
            ],
          ),
        ),
      ],
    ));
  }
}
