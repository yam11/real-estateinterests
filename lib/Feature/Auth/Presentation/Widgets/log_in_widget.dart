import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:page_transition/page_transition.dart';
import 'package:senior/Core/Constants/app_assets.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Widgets/dialog_loading.dart';
import 'package:senior/Feature/Auth/Bloc/bloc/auth_bloc.dart';
import 'package:senior/Core/Widgets/coustom_text_falid.dart';
import 'package:senior/Feature/Main/Presentation/Pages/main_page.dart';
import 'package:sizer/sizer.dart';

class LogInPage extends StatelessWidget {
  LogInPage({required this.authBloc, Key? key}) : super(key: key);

  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  FocusNode userNameFocus = FocusNode();
  FocusNode passwordFocus = FocusNode();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  AuthBloc authBloc;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: SingleChildScrollView(
        physics: const BouncingScrollPhysics(
            parent: AlwaysScrollableScrollPhysics()),
        child: Column(
          children: [
            SizedBox(
              height: 15.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 3.w,
              ),
              child: CustomTextField(
                isPassword: false,
                iconColor: AppColors.getPrimarySeconedary(),
                fillColor: AppColors.getBlackWhite(),
                prefixIconImage: AppAssets.emailIcon,
                hintText: "Email",
                focusNode: userNameFocus,
                textInputAction: TextInputAction.next,
                textInputType: TextInputType.emailAddress,
                controller: userNameController,
                isValidator: true,
                validatorMessage: "Enter Your Email",
              ),
            ),
            SizedBox(
              height: 1.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 3.w,
              ),
              child: CustomTextField(
                isPassword: true,
                iconColor: AppColors.getPrimarySeconedary(),
                fillColor: AppColors.getBlackWhite(),
                prefixIconImage: AppAssets.lockIcon,
                hintText: "password",
                focusNode: passwordFocus,
                textInputAction: TextInputAction.next,
                textInputType: TextInputType.visiblePassword,
                controller: passwordController,
                isValidator: true,
                validatorMessage: "Enter Your Password",
              ),
            ),
            SizedBox(
              height: 5.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 4.w,
              ),
              child: BlocListener<AuthBloc, AuthState>(
                listener: (context, state) {
                  if (state is SuccessToLogIn) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      duration: const Duration(seconds: 2),
                      content: Row(
                        children: [
                          Text(
                            "LogIn Success",
                            style: TextStyle(
                                color: AppColors.whiteColor, fontSize: 11.sp),
                          ),
                          SizedBox(
                            width: 4.w,
                          ),
                        ],
                      ),
                      backgroundColor: (Colors.green),
                    ));
                    Navigator.of(context).pushAndRemoveUntil(
                        PageTransition(
                            child: MainPage(),
                            type: PageTransitionType.rightToLeft,
                            duration: const Duration(milliseconds: 400)),
                        ModalRoute.withName("/"));
                  }
                  if (state is LoadingToLogIn) {
                    showLoaderDialog(context);
                  }
                  if (state is ErrorToLogIn) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      duration: const Duration(seconds: 2),
                      content: Row(
                        children: [
                          Text(
                            state.message,
                            style: TextStyle(
                                color: AppColors.whiteColor, fontSize: 11.sp),
                          ),
                          SizedBox(
                            width: 4.w,
                          ),
                        ],
                      ),
                      backgroundColor: (Colors.red),
                    ));
                    Navigator.of(context).pop();
                  }
                },
                child: MaterialButton(
                    height: 5.h,
                    color: AppColors.getPrimarySeconedary(),
                    minWidth: double.infinity,
                    onPressed: () {
                      if (formKey.currentState!.validate()) {
                        authBloc.add(LogInEvent(
                            userName: userNameController.text,
                            password: passwordController.text));
                      }
                    },
                    child: Text(
                      "Log In",
                      style: TextStyle(
                          color: AppColors.getBlackWhite(),
                          fontSize: 15.sp,
                          fontWeight: FontWeight.bold),
                    )),
              ),
            )
          ],
        ),
      ),
    );
  }
}
