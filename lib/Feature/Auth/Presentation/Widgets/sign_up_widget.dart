import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:page_transition/page_transition.dart';
import 'package:senior/Core/Constants/app_assets.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Widgets/coustom_text_falid.dart';
import 'package:senior/Core/Widgets/dialog_loading.dart';
import 'package:senior/Feature/Auth/Bloc/bloc/auth_bloc.dart';
import 'package:senior/Feature/Main/Presentation/Pages/main_page.dart';
import 'package:sizer/sizer.dart';

class SignUpPage extends StatelessWidget {
  SignUpPage({required this.authBloc, super.key});

  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  FocusNode firstNameNode = FocusNode();
  FocusNode lastNameNode = FocusNode();
  FocusNode emailNode = FocusNode();
  FocusNode phoneNumberNode = FocusNode();
  FocusNode passwordNode = FocusNode();

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  AuthBloc authBloc;
  @override
  Widget build(BuildContext context) {
    return Form(
        key: formKey,
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(
              parent: AlwaysScrollableScrollPhysics()),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 1.w),
            child: Column(
              children: [
                SizedBox(
                  height: 5.h,
                ),
                Row(
                  children: [
                    Expanded(
                      child: CustomTextField(
                        isPassword: false,
                        iconColor: AppColors.getPrimarySeconedary(),
                        fillColor: AppColors.getBlackWhite(),
                        hintText: "First Name",
                        focusNode: firstNameNode,
                        textInputAction: TextInputAction.next,
                        textInputType: TextInputType.name,
                        controller: firstNameController,
                        isValidator: true,
                        validatorMessage: "Enter Your first name",
                      ),
                    ),
                    SizedBox(
                      width: 1.w,
                    ),
                    Expanded(
                      child: CustomTextField(
                        isPassword: false,
                        iconColor: AppColors.getPrimarySeconedary(),
                        fillColor: AppColors.getBlackWhite(),
                        hintText: "Last Name",
                        focusNode: lastNameNode,
                        textInputAction: TextInputAction.next,
                        textInputType: TextInputType.name,
                        controller: lastNameController,
                        isValidator: true,
                        validatorMessage: "Enter Your last name",
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                CustomTextField(
                  isPassword: false,
                  iconColor: AppColors.getPrimarySeconedary(),
                  fillColor: AppColors.getBlackWhite(),
                  prefixIconImage: AppAssets.emailIcon,
                  hintText: "Email",
                  focusNode: emailNode,
                  textInputAction: TextInputAction.next,
                  textInputType: TextInputType.emailAddress,
                  controller: emailController,
                  isValidator: true,
                  validatorMessage: "Enter Your Email",
                ),
                SizedBox(
                  height: 2.h,
                ),
                CustomTextField(
                  isPassword: false,
                  iconColor: AppColors.getPrimarySeconedary(),
                  fillColor: AppColors.getBlackWhite(),
                  prefixIconImage: AppAssets.phoneIcon,
                  hintText: "Phone Number",
                  focusNode: phoneNumberNode,
                  textInputAction: TextInputAction.next,
                  textInputType: TextInputType.phone,
                  controller: phoneNumberController,
                  isValidator: true,
                  validatorMessage: "Enter Your Phone",
                ),
                SizedBox(
                  height: 2.h,
                ),
                CustomTextField(
                  isPassword: true,
                  iconColor: AppColors.getPrimarySeconedary(),
                  fillColor: AppColors.getBlackWhite(),
                  prefixIconImage: AppAssets.lockIcon,
                  hintText: "Password",
                  focusNode: passwordNode,
                  textInputAction: TextInputAction.next,
                  textInputType: TextInputType.visiblePassword,
                  controller: passwordController,
                  isValidator: true,
                  validatorMessage: "Enter Your Password",
                ),
                SizedBox(
                  height: 5.h,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 4.w,
                  ),
                  child: BlocListener<AuthBloc, AuthState>(
                    listener: (context, state) {
                      if (state is SuccessToLogIn) {
                        firstNameController.clear();
                        lastNameController.clear();
                        emailController.clear();
                        phoneNumberController.clear();
                        passwordController.clear();
                        Navigator.of(context).pop();
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          duration: const Duration(seconds: 2),
                          content: Row(
                            children: [
                              Text(
                                "SignUp Success",
                                style: TextStyle(
                                    color: AppColors.whiteColor,
                                    fontSize: 11.sp),
                              ),
                              SizedBox(
                                width: 4.w,
                              ),
                            ],
                          ),
                          backgroundColor: (Colors.green),
                        ));
                        authBloc.tabController.animateTo(0);
                      }
                      if (state is LoadingToLogIn) {
                        showLoaderDialog(context);
                      }
                      if (state is ErrorToLogIn) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          duration: const Duration(seconds: 2),
                          content: Row(
                            children: [
                              Text(
                                state.message,
                                style: TextStyle(
                                    color: AppColors.whiteColor,
                                    fontSize: 11.sp),
                              ),
                              SizedBox(
                                width: 4.w,
                              ),
                            ],
                          ),
                          backgroundColor: (Colors.red),
                        ));
                        Navigator.of(context).pop();
                      }
                    },
                    child: MaterialButton(
                        height: 5.h,
                        color: AppColors.getPrimarySeconedary(),
                        minWidth: double.infinity,
                        onPressed: () {
                          if (formKey.currentState!.validate()) {
                            authBloc.add(SignUpEvent(
                                email: emailController.text,
                                firstName: firstNameController.text,
                                lastName: lastNameController.text,
                                phone: phoneNumberController.text,
                                password: passwordController.text));
                          }
                        },
                        child: Text(
                          "SignUp",
                          style: TextStyle(
                              color: AppColors.getBlackWhite(),
                              fontSize: 15.sp,
                              fontWeight: FontWeight.bold),
                        )),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
