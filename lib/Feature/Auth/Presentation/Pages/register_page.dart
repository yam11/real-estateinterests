import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senior/App/Bloc/app_theme_bloc/app_theme_bloc.dart';
import 'package:senior/App/app_localizations.dart';
import 'package:senior/Core/Constants/app_assets.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Feature/Auth/Bloc/bloc/auth_bloc.dart';
import 'package:senior/Feature/Auth/Presentation/Widgets/log_in_widget.dart';
import 'package:senior/Feature/Auth/Presentation/Widgets/sign_up_widget.dart';
import 'package:sizer/sizer.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage>
    with SingleTickerProviderStateMixin {
  AuthBloc authBloc = AuthBloc();

  @override
  void initState() {
    super.initState();
    authBloc.tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppThemeBloc, ChangeTheme>(
      builder: (context, state) {
        return BlocProvider(
          create: (context) => authBloc,
          child: Scaffold(
            backgroundColor: AppColors.getPrimarySeconedary(),
            extendBody: true,
            extendBodyBehindAppBar: true,
            body: SafeArea(
              child: Column(children: [
                Image(
                  image: const AssetImage(
                    AppAssets.splashImage,
                  ),
                  height: 20.h,
                  width: 50.w,
                ),
                Expanded(
                    child: Container(
                        decoration: BoxDecoration(
                            color: AppColors.getBlackLightWhite(),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.w),
                                topRight: Radius.circular(10.w))),
                        child: Column(
                          children: [
                            TabBar(
                                controller: authBloc.tabController,

                                // isScrollable: true,
                                physics: const BouncingScrollPhysics(
                                    parent: AlwaysScrollableScrollPhysics()),
                                indicatorPadding:
                                    EdgeInsets.symmetric(horizontal: 10.w),
                                // controller: tabController,

                                labelColor: AppColors.getPrimarySeconedary(),
                                unselectedLabelColor: AppColors.getWhiteGrey(),
                                indicatorSize: TabBarIndicatorSize.tab,
                                indicatorColor: AppColors.primaryColor,
                                tabs: [
                                  Tab(
                                    child: Text("LogIn".tr(context),
                                        overflow: TextOverflow.ellipsis),
                                  ),
                                  Tab(
                                    child: Text("SignUp".tr(context),
                                        overflow: TextOverflow.ellipsis),
                                  ),
                                ]),
                            Expanded(
                                child: TabBarView(
                              controller: authBloc.tabController,
                              physics: const BouncingScrollPhysics(
                                  parent: AlwaysScrollableScrollPhysics()),
                              children: [
                                LogInPage(authBloc: authBloc),
                                SignUpPage(authBloc: authBloc)
                              ],
                            ))
                          ],
                        )))
              ]),
            ),
          ),
        );
      },
    );
  }
}
