part of 'auth_bloc.dart';

@immutable
abstract class AuthState {}

class AuthInitial extends AuthState {}

class SuccessToLogIn extends AuthState {}

class ErrorToLogIn extends AuthState {
  final String message;
  ErrorToLogIn({required this.message});
}

class LoadingToLogIn extends AuthState {}
