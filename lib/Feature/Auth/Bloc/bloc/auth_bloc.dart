import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:senior/Core/Api/ExceptionsHandle.dart';
import 'package:senior/Core/Api/Network.dart';
import 'package:senior/Core/Api/Urls.dart';
import 'package:senior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  late TabController tabController;
  AuthBloc() : super(AuthInitial()) {
    on<AuthEvent>((event, emit) async {
      if (event is LogInEvent) {
        emit(LoadingToLogIn());
        try {
          await Network.postData(url: Urls.login, data: {
            "username": event.userName,
            "password": event.password
          }).then((value) async {
            await AppSharedPreferences.saveToken(value.data['token']);
            await AppSharedPreferences.saveUserName(event.userName);
          });

          emit(SuccessToLogIn());
        } catch (error) {
          if (error is DioError) {
            if (error.response!.statusCode == 401) {
              emit(ErrorToLogIn(message: "Check your username or password"));
            }
            print("what is the $error");
            emit(ErrorToLogIn(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToLogIn(message: error.toString()));
          }
        }
      }
      if (event is SignUpEvent) {
        emit(LoadingToLogIn());
        try {
          await Network.postData(url: Urls.signUp, data: {
            "firstName": event.firstName,
            "lastName": event.lastName,
            "username": event.email,
            "mobileNumber": event.phone,
            "password": event.password,
            "walletAddress": "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa"
          });

          emit(SuccessToLogIn());
        } catch (error) {
          if (error is DioError) {
            if (error.response!.statusCode == 401) {
              emit(ErrorToLogIn(message: "Check your username or password"));
            }
            print("what is the $error");
            emit(ErrorToLogIn(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToLogIn(message: error.toString()));
          }
        }
      }
    });
  }
}
