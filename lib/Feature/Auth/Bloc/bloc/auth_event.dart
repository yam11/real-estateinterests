// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent {}

class LogInEvent extends AuthEvent {
  final String userName;
  final String password;
  LogInEvent({
    required this.userName,
    required this.password,
  });
}

class SignUpEvent extends AuthEvent {
  final String email;
  final String firstName;
  final String lastName;
  final String phone;
  final String password;
  SignUpEvent({
    required this.email,
    required this.firstName,
    required this.lastName,
    required this.phone,
    required this.password,
  });
}
