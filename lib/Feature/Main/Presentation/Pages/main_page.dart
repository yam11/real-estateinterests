import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:page_transition/page_transition.dart';
import 'package:senior/App/Bloc/app_theme_bloc/app_theme_bloc.dart';
import 'package:senior/App/Bloc/connectivity_bloc/connectivity_bloc.dart';
import 'package:senior/App/Bloc/scroll_bloc/scroll_bloc.dart';
import 'package:senior/App/app_localizations.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:senior/Core/Widgets/app_bar_widget.dart';
import 'package:senior/Core/Widgets/back_graound_widget.dart';
import 'package:senior/Core/Widgets/pub_up_dialog.dart';
import 'package:senior/Core/Widgets/scroll_widget.dart';
import 'package:senior/Feature/Favorite/Bloc/bloc/favorite_bloc.dart';
import 'package:senior/Feature/Favorite/Presentation/Pages/favorite_page.dart';
import 'package:senior/Feature/Home/Bloc/home_bloc/home_bloc.dart';
import 'package:senior/Feature/Home/Presentation/Pages/home_page.dart';
import 'package:senior/Feature/Profile/Bloc/profile_bloc/profile_bloc.dart';
import 'package:senior/Feature/Profile/Presentation/Pages/profile_page.dart';
import 'package:senior/Feature/Setting/Presentation/Pages/setting_page.dart';
import 'package:senior/Feature/Splash/Presentation/Pages/splash_page.dart';
import 'package:sizer/sizer.dart';

class MainPage extends StatefulWidget {
  MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  @override
  void initState() {
    tabController = TabController(
      vsync: this,
      length: 4,
    );
    // NetworkInfo.checkConnectivity(context);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    tabController.dispose();
  }

  bool show = false;
  final HomeBloc homeBloc = HomeBloc();
  final ProfileBloc profileBloc = ProfileBloc();
  final FavoriteBloc favoriteBloc = FavoriteBloc();

  @override
  Widget build(BuildContext context) {
    tabController.addListener(() {
      if (tabController.index == 3) {
        setState(() {
          show = true;
        });
      } else {
        setState(() {
          show = false;
        });
      }
    });
    return BlocListener<ConnectivityBloc, ConnectedState>(
      listener: (context, state) {
        if (state.message == "Connecting To Wifi") {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            backgroundColor: Colors.green,
            duration: const Duration(seconds: 5),
            content: Text(
              state.message.tr(context),
              textAlign: AppSharedPreferences.getArLang == "en"
                  ? TextAlign.start
                  : TextAlign.end,
            ),
          ));
        }
        if (state.message == "Connecting To Mobile Data") {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            backgroundColor: Colors.green,
            duration: const Duration(seconds: 5),
            content: Text(
              state.message.tr(context),
              textAlign: AppSharedPreferences.getArLang == "en"
                  ? TextAlign.start
                  : TextAlign.end,
            ),
          ));
        }
        if (state.message == "Lost Internet Connection") {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            backgroundColor: Colors.red,
            duration: const Duration(seconds: 5),
            content: Text(
              state.message.tr(context),
              textAlign: AppSharedPreferences.getArLang == "en"
                  ? TextAlign.start
                  : TextAlign.end,
            ),
          ));
        }
      },
      child: BlocBuilder<AppThemeBloc, ChangeTheme>(
        builder: (context, state) {
          SystemChrome.setSystemUIOverlayStyle(
            SystemUiOverlayStyle(
              statusBarColor: AppColors.getPrimarySeconedary()
                  .withOpacity(0.5), // Set the status bar color
              statusBarIconBrightness: AppSharedPreferences.hasDarkTheme
                  ? Brightness.dark
                  : Brightness.light, // Set the status bar icon color
            ),
          );
          return Directionality(
            textDirection: AppSharedPreferences.getArLang == "ar"
                ? TextDirection.rtl
                : TextDirection.ltr,
            child: Scaffold(
                floatingActionButton: show
                    ? FloatingActionButton.extended(
                        backgroundColor: AppColors.getPrimarySeconedary(),
                        foregroundColor: Colors.black,
                        onPressed: () {
                          showYesNoDialog(context, "Are you sure ?", () async {
                            await AppSharedPreferences.removeUserName();
                            await AppSharedPreferences.removeToken();
                            if (context.mounted) {
                              Navigator.of(context).pushAndRemoveUntil(
                                  PageTransition(
                                      child: SplashPage(),
                                      type: PageTransitionType.rightToLeft,
                                      duration:
                                          const Duration(milliseconds: 300)),
                                  ModalRoute.withName("/"));
                            }
                          }, () {
                            Navigator.of(context).pop();
                          });
                        },
                        icon: Icon(
                          Icons.logout,
                          color: AppColors.getBlackWhite(),
                        ),
                        label: Text(
                          "Log Out".tr(context),
                          style: TextStyle(
                              color: AppColors.getBlackWhite(),
                              fontSize: 15.sp,
                              fontWeight: FontWeight.bold),
                        ),
                      )
                    : null,
                backgroundColor: AppColors.getBlackLightWhite(),
                extendBody: true,
                extendBodyBehindAppBar: true,
                appBar: PreferredSize(
                  preferredSize: Size.fromHeight(AppBar().preferredSize.height),
                  child: BlocBuilder<ScrollBloc, ScrollState>(
                    builder: (context, state) {
                      return SafeArea(
                        child: AnimatedContainer(
                            height: context.read<ScrollBloc>().appBar
                                ? AppBar().preferredSize.height
                                : Size.zero.height,
                            duration: const Duration(milliseconds: 400),
                            child: AppBarWidget(
                              buttonBack: false,
                              title: "Real EstateInterests".tr(context),
                            )),
                      );
                    },
                  ),
                ),
                body: SafeArea(
                  child: Column(children: [
                    Container(
                      width: double.infinity,
                      color: AppColors.getBlackGrey(),
                      alignment: Alignment.center,
                      child: TabBar(
                          controller: tabController,
                          isScrollable: true,
                          physics: const BouncingScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          // controller: tabController,

                          labelColor: AppColors.getPrimarySeconedary(),
                          unselectedLabelColor: AppColors.getGreyWhite(),
                          indicatorSize: TabBarIndicatorSize.tab,
                          indicatorColor: AppColors.getPrimarySeconedary(),
                          tabs: [
                            Tab(
                              child: Column(
                                children: [
                                  const Icon(Icons.home),
                                  Expanded(
                                    child: Text("Home".tr(context),
                                        overflow: TextOverflow.ellipsis),
                                  ),
                                ],
                              ),
                            ),
                            Tab(
                              child: Column(
                                children: [
                                  const Icon(Icons.favorite),
                                  Expanded(
                                    child: Text("Favorite".tr(context),
                                        overflow: TextOverflow.ellipsis),
                                  ),
                                ],
                              ),
                            ),
                            Tab(
                              child: Column(
                                children: [
                                  const Icon(Icons.person),
                                  Expanded(
                                    child: Text("Profile".tr(context),
                                        overflow: TextOverflow.ellipsis),
                                  ),
                                ],
                              ),
                            ),
                            Tab(
                              child: Column(
                                children: [
                                  const Icon(Icons.settings),
                                  Expanded(
                                    child: Text("Settings".tr(context),
                                        overflow: TextOverflow.ellipsis),
                                  ),
                                ],
                              ),
                            ),
                          ]),
                    ),
                    MultiBlocProvider(
                      providers: [
                        BlocProvider(
                          create: (context) => homeBloc..add(GetAllPosts()),
                        ),
                        BlocProvider(
                            create: (context) => profileBloc..add(MyProfile())),
                        BlocProvider(
                            create: (context) =>
                                favoriteBloc..add(GetAllFavorite())),
                      ],
                      child: Expanded(
                          child: TabBarView(
                        controller: tabController,
                        children: [
                          HomePage(homeBloc: homeBloc),
                          FavoritePage(
                            favoriteBloc: favoriteBloc,
                          ),
                          ProfilePage(profileBloc: profileBloc),
                          const ScrollWidget(child: SettingPage())
                        ],
                      )),
                    )
                  ]),
                )),
          );
        },
      ),
    );
  }
}
