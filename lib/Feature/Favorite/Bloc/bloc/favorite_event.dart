part of 'favorite_bloc.dart';

@immutable
abstract class FavoriteEvent {}

class GetAllFavorite extends FavoriteEvent {}

class AddFavorite extends FavoriteEvent {
  final String postId;
  AddFavorite({required this.postId});
}
