part of 'favorite_bloc.dart';

@immutable
abstract class FavoriteState {}

class FavoriteInitial extends FavoriteState {}

class SuccessToGetAllFavorite extends FavoriteState {}

class LoadingToGetAllFavorite extends FavoriteState {}

class ErrorToGetAllFavorite extends FavoriteState {
  final String message;
  ErrorToGetAllFavorite({required this.message});
}

class SuccessAddToFavorite extends FavoriteState {}

class LoadingAddToFavorite extends FavoriteState {}

class ErrorAddToFavorite extends FavoriteState {
  final String message;
  ErrorAddToFavorite({required this.message});
}
