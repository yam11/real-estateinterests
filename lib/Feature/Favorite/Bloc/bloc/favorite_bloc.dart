import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:senior/Core/Api/ExceptionsHandle.dart';
import 'package:senior/Core/Api/Network.dart';
import 'package:senior/Core/Api/Urls.dart';
import 'package:senior/Feature/Favorite/Models/favorites_model.dart';
import 'package:senior/Feature/Home/Models/all_posts_model.dart';

part 'favorite_event.dart';
part 'favorite_state.dart';

class FavoriteBloc extends Bloc<FavoriteEvent, FavoriteState> {
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  FavoritesModel? listOfFavoritesModel;
  FavoriteBloc() : super(FavoriteInitial()) {
    on<FavoriteEvent>((event, emit) async {
      if (event is GetAllFavorite) {
        emit(LoadingToGetAllFavorite());
        try {
          await Network.getData(url: Urls.favorites).then((value) {
            listOfFavoritesModel = FavoritesModel.fromJson(value.data);
          });
          emit(SuccessToGetAllFavorite());
        } catch (error) {
          print("what is the error : $error");
          if (error is DioError) {
            emit(
                ErrorToGetAllFavorite(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetAllFavorite(message: "error"));
          }
        }
      }
    });
  }
}
