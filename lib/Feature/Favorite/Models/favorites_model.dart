import 'package:senior/Feature/Home/Models/all_posts_model.dart';

class FavoritesModel {
  Favorites? favorites;

  FavoritesModel({this.favorites});

  FavoritesModel.fromJson(Map<String, dynamic> json) {
    favorites = json['favorites'] != null
        ? new Favorites.fromJson(json['favorites'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.favorites != null) {
      data['favorites'] = this.favorites!.toJson();
    }
    return data;
  }
}

class Favorites {
  List<RealEstate>? realEstate;
  String? sId;
  Owner? user;
  String? createdAt;
  String? updatedAt;
  int? iV;

  Favorites(
      {this.realEstate,
      this.sId,
      this.user,
      this.createdAt,
      this.updatedAt,
      this.iV});

  Favorites.fromJson(Map<String, dynamic> json) {
    if (json['realEstate'] != null) {
      realEstate = <RealEstate>[];
      json['realEstate'].forEach((v) {
        realEstate!.add(new RealEstate.fromJson(v));
      });
    }
    sId = json['_id'];
    user = json['user'] != null ? new Owner.fromJson(json['user']) : null;
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.realEstate != null) {
      data['realEstate'] = this.realEstate!.map((v) => v.toJson()).toList();
    }
    data['_id'] = this.sId;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class RealEstate {
  Coordinates? coordinates;
  List<String>? images;
  String? label;
  bool? featured;
  String? sId;
  String? location;
  String? description;
  String? category;
  int? price;
  Owner? owner;
  List<Comments>? comments;
  String? createdAt;
  String? updatedAt;
  int? iV;

  RealEstate(
      {this.coordinates,
      this.images,
      this.label,
      this.featured,
      this.sId,
      this.location,
      this.description,
      this.category,
      this.price,
      this.owner,
      this.comments,
      this.createdAt,
      this.updatedAt,
      this.iV});

  RealEstate.fromJson(Map<String, dynamic> json) {
    coordinates = json['coordinates'] != null
        ? new Coordinates.fromJson(json['coordinates'])
        : null;
    images = json['images'].cast<String>();
    label = json['label'];
    featured = json['featured'];
    sId = json['_id'];
    location = json['location'];
    description = json['description'];
    category = json['category'];
    price = json['price'];
    owner = json['owner'] != null ? new Owner.fromJson(json['owner']) : null;
    if (json['comments'] != null) {
      comments = <Comments>[];
      json['comments'].forEach((v) {
        comments!.add(new Comments.fromJson(v));
      });
    }
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.coordinates != null) {
      data['coordinates'] = this.coordinates!.toJson();
    }
    data['images'] = this.images;
    data['label'] = this.label;
    data['featured'] = this.featured;
    data['_id'] = this.sId;
    data['location'] = this.location;
    data['description'] = this.description;
    data['category'] = this.category;
    data['price'] = this.price;
    if (this.owner != null) {
      data['owner'] = this.owner!.toJson();
    }
    if (this.comments != null) {
      data['comments'] = this.comments!.map((v) => v.toJson()).toList();
    }
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class Coordinates {
  String? type;
  List<double>? coordinates;

  Coordinates({this.type, this.coordinates});

  Coordinates.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    coordinates = json['coordinates'].cast<double>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['coordinates'] = this.coordinates;
    return data;
  }
}



class Comments {
  String? sId;
  int? rating;
  String? comment;
  String? author;
  String? createdAt;
  String? updatedAt;

  Comments(
      {this.sId,
      this.rating,
      this.comment,
      this.author,
      this.createdAt,
      this.updatedAt});

  Comments.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    rating = json['rating'];
    comment = json['comment'];
    author = json['author'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['rating'] = this.rating;
    data['comment'] = this.comment;
    data['author'] = this.author;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}
