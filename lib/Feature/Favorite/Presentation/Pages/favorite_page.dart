import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:senior/App/app_localizations.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Widgets/error_message_widget.dart';
import 'package:senior/Core/Widgets/loading_widget.dart';
import 'package:senior/Core/Widgets/scroll_widget.dart';
import 'package:senior/Feature/Favorite/Bloc/bloc/favorite_bloc.dart';
import 'package:senior/Feature/Favorite/Presentation/Widgets/post_favorites_widget.dart';
import 'package:senior/Feature/Home/Presentation/Widgets/post_widget.dart';
import 'package:sizer/sizer.dart';

class FavoritePage extends StatelessWidget {
  final FavoriteBloc favoriteBloc;
  const FavoritePage({required this.favoriteBloc, super.key});

  @override
  Widget build(BuildContext context) {
    return ScrollWidget(
      child: BlocConsumer<FavoriteBloc, FavoriteState>(
        listener: (context, state) {},
        builder: (context, state) {
          if (state is ErrorToGetAllFavorite) {
            return ErrorMessageWidget(
              message: state.message,
              onPressed: () {
                favoriteBloc.add(GetAllFavorite());
              },
            );
          }
          if (state is SuccessToGetAllFavorite) {
            return SmartRefresher(
              controller: favoriteBloc.refreshController,
              onRefresh: () async {
                favoriteBloc.add(GetAllFavorite());
              },
              physics: const BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics()),
              child: favoriteBloc
                      .listOfFavoritesModel!.favorites!.realEstate!.isNotEmpty
                  ? ListView.builder(
                      shrinkWrap: true,
                      physics: const BouncingScrollPhysics(
                          parent: AlwaysScrollableScrollPhysics()),
                      itemCount: favoriteBloc
                          .listOfFavoritesModel!.favorites!.realEstate!.length,
                      itemBuilder: (context, index) => PostFavoritesWidget(
                          listOfPostsModel: favoriteBloc.listOfFavoritesModel!,
                          index: index),
                    )
                  : Center(
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 5.w, vertical: 5.h),
                        decoration: BoxDecoration(
                            border: Border.all(
                                width: 1,
                                color: AppColors.getPrimarySeconedary()),
                            borderRadius: BorderRadius.circular(5.w)),
                        child: Text(
                          "Favorites is empty".tr(context),
                          style: TextStyle(
                              color: AppColors.getPrimarySeconedary(),
                              fontSize: 16.sp,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
            );
          } else {
            return Center(child: LoadingWidget());
          }
        },
      ),
    );
  }
}
