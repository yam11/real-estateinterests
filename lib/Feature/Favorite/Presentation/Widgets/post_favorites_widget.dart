import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:senior/App/app_localizations.dart';
import 'package:senior/Core/Constants/app_assets.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Feature/Favorite/Models/favorites_model.dart';
import 'package:senior/Feature/Favorite/Presentation/Widgets/single_post_favorites.dart';
import 'package:senior/Feature/Home/Presentation/Widgets/show_bottom_sheet.dart';
import 'package:senior/Feature/Home/Presentation/Widgets/avatar_view_widget.dart';
import 'package:senior/Feature/Home/Presentation/Widgets/favorite_widget.dart';
import 'package:senior/Feature/Home/Presentation/Widgets/slider_Images_widget.dart';
import 'package:sizer/sizer.dart';
import 'dart:ui' as ui;

class PostFavoritesWidget extends StatelessWidget {
  FavoritesModel listOfPostsModel;
  int index;
  PostFavoritesWidget(
      {required this.listOfPostsModel, required this.index, super.key});

  bool isArabic(String text) {
    RegExp arabicRegex = RegExp(
        r'[\u0600-\u06FF\u0750-\u077F\u08A0-\u08FF\uFB50-\uFDFD\uFE70-\uFEFF]+');
    return arabicRegex.hasMatch(text);
  }

  @override
  Widget build(BuildContext context) {
    print("what is the model $listOfPostsModel");
    return Container(
      margin: EdgeInsets.only(bottom: 1.h),
      color: AppColors.getBlackLightWhite(),
      child: Column(children: [
        Stack(
          children: [
            SliderImagesWidget(
              images: listOfPostsModel.favorites!.realEstate![index].images!,
            ),
            Padding(
              padding: EdgeInsets.only(
                  left: 4.w,
                  top:  listOfPostsModel.favorites!.realEstate![index].images!.isNotEmpty
                      ? 35.h
                      : 0),
              child: Row(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (_) => const AvatarViewWidget(
                                image: AppAssets.splashImage,
                              )));
                    },
                    child: CircleAvatar(
                        // backgroundImage: NetworkImage(widget.avatarImage),
                        radius: 10.w,
                        backgroundColor: AppColors.greyColor,
                        // foregroundColor: Colors.brown.withOpacity(0.5),
                        child: Icon(
                          Icons.person,
                          size: 25.sp,
                          color: AppColors.white2Color,
                        )),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 5.h),
                    child: Text(
                       listOfPostsModel.favorites!.realEstate![index].owner!.firstName!,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 16.sp,
                          color: Colors.grey,
                          fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        SizedBox(
          height: 2.h,
        ),
        Directionality(
          textDirection:
              isArabic( listOfPostsModel.favorites!.realEstate![index].description!)
                  ? ui.TextDirection.rtl
                  : ui.TextDirection.ltr,
          child: Row(
            children: [
              Expanded(
                child: Text(
                   listOfPostsModel.favorites!.realEstate![index].description!,
                  maxLines: 3,
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  style:
                      TextStyle(color: AppColors.textColor(), fontSize: 14.sp),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 2.h,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 5.w),
          child: Divider(
            thickness: 1,
            color: AppColors.getPrimarySeconedary(),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).push(
                    PageTransition(
                        child: ViewSingleFavoritesPostPage(
                            postId:  listOfPostsModel.favorites!.realEstate![index].sId!,
                            owner:  listOfPostsModel.favorites!.realEstate![index].owner!),
                        type: PageTransitionType.rightToLeft,
                        duration: const Duration(milliseconds: 400)),
                  );
                },
                child: Row(
                  children: [
                    Text(
                      "read more".tr(context),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 10.sp,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey),
                    ),
                    SizedBox(
                      width: 1.w,
                    ),
                    Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.grey,
                      size: 12.sp,
                    )
                  ],
                )),
            Row(
              children: [
                InkWell(
                  onTap: () {
                    commentBottomSheet(
                        context,  listOfPostsModel.favorites!.realEstate![index].sId!);
                  },
                  child: Container(
                      height: 4.h,
                      width: 22.w,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: AppColors.primaryColor.withOpacity(0.25),
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        "Comments".tr(context),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 10.sp,
                            fontWeight: FontWeight.bold),
                      )),
                ),
                 FavoriteWidget(isAdd: false, postId: listOfPostsModel.favorites!.realEstate![index].sId!,)
              ],
            ),
          ],
        ),
      ]),
    );
  }
}
