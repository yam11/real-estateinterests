import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:senior/App/app_localizations.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:senior/Core/Widgets/error_message_widget.dart';
import 'package:senior/Core/Widgets/loading_widget.dart';
import 'package:senior/Core/Widgets/scroll_widget.dart';
import 'package:senior/Feature/AddNewPost/Presentation/Pages/add_new_post.dart';
import 'package:senior/Feature/Home/Presentation/Widgets/dilog_conf.dart';
import 'package:senior/Feature/Profile/Bloc/add_wallet_bloc_bloc/add_wallet_bloc_bloc.dart';
import 'package:senior/Feature/Profile/Bloc/profile_bloc/profile_bloc.dart';
import 'package:sizer/sizer.dart';
import 'package:walletconnect_dart/walletconnect_dart.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({required this.profileBloc, super.key});
  final ProfileBloc profileBloc;

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  void initState() {
    super.initState();
  }

  var connector = WalletConnect(
      bridge: 'https://bridge.walletconnect.org',
      clientMeta: const PeerMeta(
          name: 'My App',
          description: 'An app for converting pictures to NFT',
          url: 'https://walletconnect.org',
          icons: [
            'https://files.gitbook.com/v0/b/gitbook-legacy-files/o/spaces%2F-LJJeCjcLrr53DcT1Ml7%2Favatar.png?alt=media'
          ]));

  var _session, _uri;

  loginUsingMetamask(BuildContext context) async {
    if (!connector.connected) {
      try {
        SessionStatus session =
            await connector.createSession(onDisplayUri: (uri) async {
          _uri = uri;
          await launchUrl(Uri.parse(uri), mode: LaunchMode.externalApplication);
        });
        print("=============================${session.accounts[0]}");
        print("=============================${session.chainId}");
        setState(() {
          _session = session;
        });
      } catch (exp) {
        print("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww $exp");
      }
    }
  }

  AddWalletBloc addWalletBloc = AddWalletBloc();
  @override
  Widget build(BuildContext context) {
    connector.on(
        'connect',
        (session) => setState(
              () {
                _session = _session;
              },
            ));
    connector.on(
        'session_update',
        (payload) => setState(() {
              _session = payload;
              print("popop $payload");
              print(payload);
            }));
    connector.on(
        'disconnect',
        (payload) => setState(() {
              _session = null;
            }));
    return BlocProvider(
      create: (context) => addWalletBloc,
      child: ScrollWidget(
        child: BlocConsumer<ProfileBloc, ProfileState>(
          listener: (context, state) {},
          builder: (context, state) {
            if (state is ErrorToGetProfile) {
              return ErrorMessageWidget(
                  onPressed: () {
                    widget.profileBloc.add(MyProfile());
                  },
                  message: state.message);
            }
            if (state is SuccessToGetProfile) {
              AppSharedPreferences.saveUserName(
                  widget.profileBloc.myProfileModel!.user!.firstName!);
              return SmartRefresher(
                controller: widget.profileBloc.refreshController,
                onRefresh: () async {
                  widget.profileBloc.add(MyProfile());
                },
                physics: const BouncingScrollPhysics(
                    parent: AlwaysScrollableScrollPhysics()),
                child: SingleChildScrollView(
                  physics: const NeverScrollableScrollPhysics(),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 2.w),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 10.h,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Center(
                                child: Column(
                                  children: [
                                    CircleAvatar(
                                      backgroundColor:
                                          AppColors.getPrimarySeconedary(),
                                      radius: 10.w,
                                      child: Icon(
                                        Icons.person,
                                      ),
                                    ),
                                    Text(
                                      widget.profileBloc.myProfileModel!.user!
                                          .firstName!,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      widget.profileBloc.myProfileModel!.user!
                                          .lastName!,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              child: Center(
                                child: Column(
                                  children: [
                                    TextButton(
                                      onPressed: () {
                                        // dialogConf(context,
                                        //     "Your are not connecting to the wallet");
                                        Navigator.of(context)
                                            .push(MaterialPageRoute(
                                          builder: (context) =>
                                              AddNewPostWidget(),
                                        ));
                                      },
                                      child: Row(
                                        children: [
                                          Text("Add New Post".tr(context),
                                              style: TextStyle(
                                                  fontSize: 12.sp,
                                                  color: AppColors
                                                      .getPrimarySeconedary(),
                                                  fontWeight: FontWeight.bold)),
                                          SizedBox(
                                            width: 2.w,
                                          ),
                                          Icon(
                                            Icons.add_circle_outline_outlined,
                                            color: AppColors
                                                .getPrimarySeconedary(),
                                          )
                                        ],
                                      ),
                                    ),
                                    BlocBuilder<AddWalletBloc,
                                        AddWalletConnectState>(
                                      builder: (context, state) {
                                        return TextButton(
                                          onPressed: () {
                                            loginUsingMetamask(context);
                                          },
                                          child: Row(
                                            children: [
                                              Text("Connect",
                                                  style: TextStyle(
                                                      fontSize: 12.sp,
                                                      color: AppColors
                                                          .getPrimarySeconedary(),
                                                      fontWeight:
                                                          FontWeight.bold)),
                                            
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 4.h,
                        ),
                        Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 3.w, vertical: 1.h),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.w),
                                border: Border.all(
                                    width: 1,
                                    color: AppColors.getPrimarySeconedary())),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      "My Email :".tr(context),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          color:
                                              AppColors.getPrimarySeconedary(),
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Expanded(
                                      child: Text(
                                        widget.profileBloc.myProfileModel!.user!
                                            .username!,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 16.sp,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 2.h,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "My Phone :".tr(context),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          color:
                                              AppColors.getPrimarySeconedary(),
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Expanded(
                                      child: Text(
                                        widget.profileBloc.myProfileModel!.user!
                                            .mobileNumber!,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 16.sp,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            )),
                      ],
                    ),
                  ),
                ),
              );
            } else {
              return Center(
                child: LoadingWidget(),
              );
            }
          },
        ),
      ),
    );
  }
}
