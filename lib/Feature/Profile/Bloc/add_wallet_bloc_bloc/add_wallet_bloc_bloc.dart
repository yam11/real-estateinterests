import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
// import 'package:metamask/metamask.dart';

part 'add_wallet_bloc_event.dart';
part 'add_wallet_bloc_state.dart';

class AddWalletBloc extends Bloc<AddWalletConnectEvent, AddWalletConnectState> {
  static const operatingChain = 4;
  String currentAddress = '';
  var account = "";
  int currentChain=-1;
  // bool get isEnabled => ethereum != null;
  bool get isInOperatingChain => currentChain == operatingChain;
  // bool get isConnected => isEnabled && currentAddress.isNotEmpty;
  AddWalletBloc() : super(AddWalletConnectState()) {
    on<AddWalletConnect>((event, emit) async {
      // if (isEnabled) {
      //   final accs = await ethereum!.requestAccount();
      //   account = accs[0];
      //   if (accs.isNotEmpty) currentAddress = accs.first;
      //   currentChain = await ethereum!.getChainId();
      //   emit(AddWalletConnectState());
      // }
    });
  }
}
