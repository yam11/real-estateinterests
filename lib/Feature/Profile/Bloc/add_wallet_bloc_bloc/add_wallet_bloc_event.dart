part of 'add_wallet_bloc_bloc.dart';

@immutable
abstract class AddWalletConnectEvent {}

class AddWalletConnect extends AddWalletConnectEvent {}