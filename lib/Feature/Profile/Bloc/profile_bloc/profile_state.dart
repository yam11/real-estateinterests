part of 'profile_bloc.dart';

@immutable
abstract class ProfileState {}

class ProfileInitial extends ProfileState {}

class LoadingToGetProfile extends ProfileState {}

class SuccessToGetProfile extends ProfileState {}

class ErrorToGetProfile extends ProfileState {
  final String message;
  ErrorToGetProfile({required this.message});
}
