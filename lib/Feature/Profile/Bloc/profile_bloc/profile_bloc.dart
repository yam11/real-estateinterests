import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:senior/Core/Api/ExceptionsHandle.dart';
import 'package:senior/Core/Api/Network.dart';
import 'package:senior/Core/Api/Urls.dart';
import 'package:senior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:senior/Feature/Profile/Models/profile_model.dart';

part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  MyProfileModel? myProfileModel;
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  ProfileBloc() : super(ProfileInitial()) {
    on<ProfileEvent>((event, emit) async {
      if (event is MyProfile) {
        emit(LoadingToGetProfile());
        try {
          await Network.getData(url: Urls.getUserProfile).then((value) {
            myProfileModel = MyProfileModel.fromJson(value.data);
          
          });
          emit(SuccessToGetProfile());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetProfile(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetProfile(message: "error"));
          }
        }
      }
    });
  }
}
