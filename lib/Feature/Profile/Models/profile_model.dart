class MyProfileModel {
  String? status;
  bool? success;
  User? user;

  MyProfileModel({this.status, this.success, this.user});

  MyProfileModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    success = json['success'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['success'] = this.success;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class User {
  String? firstName;
  String? lastName;
  bool? admin;
  String? mobileNumber;
  String? walletAddress;
  String? sId;
  String? username;
  int? iV;

  User(
      {this.firstName,
      this.lastName,
      this.admin,
      this.mobileNumber,
      this.walletAddress,
      this.sId,
      this.username,
      this.iV});

  User.fromJson(Map<String, dynamic> json) {
    firstName = json['firstName'];
    lastName = json['lastName'];
    admin = json['admin'];
    mobileNumber = json['mobileNumber'];
    walletAddress = json['walletAddress'];
    sId = json['_id'];
    username = json['username'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['admin'] = this.admin;
    data['mobileNumber'] = this.mobileNumber;
    data['walletAddress'] = this.walletAddress;
    data['_id'] = this.sId;
    data['username'] = this.username;
    data['__v'] = this.iV;
    return data;
  }
}
