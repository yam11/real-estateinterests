import 'package:flutter/material.dart';
import 'package:senior/App/app.dart';
import 'package:senior/Core/Api/Network.dart';
import 'package:senior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppSharedPreferences.init();
  await Network.init();
  print("token is ${AppSharedPreferences.getToken}");
  print("Language is ${AppSharedPreferences.getArLang}");
    print("Dark theme is ${AppSharedPreferences.getDarkTheme}");
       print("user name is ${AppSharedPreferences.getUserName}");
  runApp(const MyApp());
}
