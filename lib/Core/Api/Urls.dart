class Urls {
  static const baseUrl = "https://10.0.2.2:3443/";
  static const login = "${baseUrl}users/login";
  static const signUp = "${baseUrl}users/signup";
  static const getAllPosts = "${baseUrl}realEstate";
  static const getAllCommentsByPostId = "${baseUrl}realEstate/";
  static const getUserProfile = "${baseUrl}users/checkJWTtoken";
   static const favorites = "${baseUrl}favorites";
   
}
