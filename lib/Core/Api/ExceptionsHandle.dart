import 'package:dio/dio.dart';

String exceptionsHandle({required DioError error}) {
  final String message;
  switch (error.type) {
    case DioErrorType.connectTimeout:
      message = 'Server not reachable';
      break;

    case DioErrorType.sendTimeout:
      message = 'Send Time out';
      break;
    case DioErrorType.receiveTimeout:
      message = 'Server not reachable';
      break;
    case DioErrorType.response:
      message = error.response!.data['message'];
      break;
    case DioErrorType.cancel:
      message = 'Request is cancelled';
      break;
    case DioErrorType.other:
      error.message.contains('SocketException')
          ? message = 'check your internet connection'
          : message = error.message;
      break;
  }

  return message;
}
