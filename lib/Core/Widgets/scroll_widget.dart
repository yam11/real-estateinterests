import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senior/App/Bloc/scroll_bloc/scroll_bloc.dart';

class ScrollWidget extends StatelessWidget {
  final Widget child;
  const ScrollWidget({required this.child, super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ScrollBloc, ScrollState>(
      builder: (context, state) {
        return NotificationListener<UserScrollNotification>(
            onNotification: (notification) {
              final ScrollDirection direction = notification.direction;

              if (direction == ScrollDirection.reverse) {
                context.read<ScrollBloc>().add(HideAppBar());
              } else if (direction == ScrollDirection.forward) {
                context.read<ScrollBloc>().add(ShowAppBar());
              }

              return true;
            },
            child: child);
      },
    );
  }
}
