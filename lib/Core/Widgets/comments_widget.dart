import 'package:flutter/material.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:senior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';

import 'package:sizer/sizer.dart';

class CommentsWidget extends StatelessWidget {
  String avatarImage;
  String comment;
  String firstName;
  String lastName;

  int index;
  CommentsWidget(
      {required this.comment,
      required this.firstName,
      required this.lastName,
      required this.index,
      required this.avatarImage,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 5.w,
      ),
      padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 2.h),
      child: Row(
        children: [
          Column(
            children: [
              CircleAvatar(
                radius: 8.w,
                backgroundColor: AppColors.primaryColor,
                // backgroundImage: AssetImage(avatarImage),
                child: Icon(Icons.person),
              ),
              SizedBox(
                  width: 20.w,
                  child: Text(
                    firstName,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: AppColors.getWhiteGrey(),
                        fontSize: 14.sp,
                        fontWeight: FontWeight.bold),
                  )),
              SizedBox(
                  width: 20.w,
                  child: Text(
                    lastName,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: AppColors.getWhiteGrey(),
                        fontSize: 14.sp,
                        fontWeight: FontWeight.bold),
                  ))
            ],
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 1.w, vertical: 1.h),
              padding: EdgeInsets.symmetric(horizontal: 1.w, vertical: 2.h),
              decoration: BoxDecoration(
                  color: AppColors.whiteColor,
                  boxShadow: [
                    BoxShadow(
                      color: const Color.fromARGB(255, 135, 152, 167)
                          .withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 3,
                      offset: const Offset(0, 3), // changes position of shadow
                    ),
                  ],
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(5.w),
                      bottomRight: Radius.circular(5.w),
                      topRight: AppSharedPreferences.hasArLang
                          ? Radius.circular(0.w)
                          : Radius.circular(5.w),
                      topLeft: AppSharedPreferences.hasArLang
                          ? Radius.circular(5.w)
                          : Radius.circular(0.w))),
              child: Text(comment,
                  style: TextStyle(
                    color: AppColors.primaryColor,
                    fontSize: 14.sp,
                  )),
            ),
          ),
        ],
      ),
    );
  }
}
