import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sizer/sizer.dart';

class MyCachedNetwork extends StatelessWidget {
  const MyCachedNetwork({
    Key? key,
    required this.imageUrl,
  }) : super(key: key);
  final String imageUrl;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      placeholder: (context, url) => Center(
        child: Shimmer.fromColors(
          baseColor: AppColors.greyColor,
          highlightColor: Colors.grey[100]!,
          child: Container(
            color: Colors.grey,
          ),
        ),
      ),
      fadeInDuration: const Duration(milliseconds: 4),
      fadeOutDuration: const Duration(milliseconds: 4),
      imageUrl: imageUrl,
      errorWidget: (context, url, error) => const Center(
          child: Icon(
        Icons.error,
        color: AppColors.white2Color,
      )),
      fit: BoxFit.fill,
    );
  }
}
