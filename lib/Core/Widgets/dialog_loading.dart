import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

showLoaderDialog(BuildContext context) {
  AlertDialog alert = AlertDialog(
    content: Row(
      children: [
        SizedBox(
          height: 5.h,
          width: 10.w,
          child: LoadingIndicator(
              indicatorType: Indicator.ballRotateChase,
              colors: [AppColors.getPrimarySeconedary()],
              strokeWidth: 4,
              backgroundColor: Colors.transparent,
              pathBackgroundColor: Colors.transparent),
        ),
        SizedBox(
          width: 2.w,
        ),
        Text(
          "Loading...",
          style: TextStyle(fontSize: 14.sp, fontWeight: FontWeight.bold),
        )
      ],
    ),
  );
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
