import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class CustomFooterWidget extends StatelessWidget {
  const CustomFooterWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return CustomFooter(
      builder: (context, mode) {
        Widget body;
        if (mode == LoadStatus.idle) {
          body = Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                height: 5.h,
                width: 10.w,
                child: const LoadingIndicator(
                    indicatorType: Indicator.ballRotateChase,
                    colors: [AppColors.primaryColor],
                    strokeWidth: 4,
                    backgroundColor: Colors.transparent,
                    pathBackgroundColor: Colors.transparent),
              ),
              Text(
                "Loading more data",
                style: TextStyle(
                    fontSize: 12.sp,
                    fontWeight: FontWeight.bold,
                    color: AppColors.primaryColor),
              )
            ],
          );
        } else if (mode == LoadStatus.loading) {
          body = const SizedBox();
        } else if (mode == LoadStatus.failed) {
          body = Text(
            "Loading Faild",
            style: TextStyle(
                fontSize: 12.sp,
                fontWeight: FontWeight.bold,
                color: AppColors.primaryColor),
          );
        } else if (mode == LoadStatus.canLoading) {
          body = Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.arrow_upward,
                color: AppColors.primaryColor,
                size: 15.sp,
              ),
              Text(
                "Pull Up",
                style: TextStyle(
                    fontSize: 12.sp,
                    fontWeight: FontWeight.bold,
                    color: AppColors.primaryColor),
              ),
            ],
          );
        } else {
          body = Text(
            "No More Data",
            style: TextStyle(
                fontSize: 12.sp,
                fontWeight: FontWeight.bold,
                color: AppColors.primaryColor),
          );
        }
        return SizedBox(
          height: 55.0,
          child: Center(child: body),
        );
      },
    );
  }
}
