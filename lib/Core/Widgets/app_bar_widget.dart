import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senior/App/Bloc/app_theme_bloc/app_theme_bloc.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class AppBarWidget extends StatelessWidget {
  final bool buttonBack;
  final String title;
  const AppBarWidget({required this.title,required this.buttonBack, super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppThemeBloc, ChangeTheme>(
      builder: (context, state) {
        return AppBar(
            centerTitle: buttonBack ? true : false,
            elevation: 0,
            backgroundColor: AppColors.getPrimarySeconedary(),
            leading: buttonBack
                ? IconButton(
                    onPressed: (() {
                      Navigator.pop(context);
                    }),
                    icon: IconButton(
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.getSeconedaryPrimary(),
                          size: 5.w,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        }),
                  )
                : null,
            title: Text(
              title
              ,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 25.sp,
                  fontWeight: FontWeight.bold,
                  color: AppColors.getSeconedaryPrimary()),
            ));
      },
    );
  }
}
