import 'package:flutter/material.dart';
import 'package:senior/Core/Constants/app_assets.dart';
import 'package:senior/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class ErrorMessageWidget extends StatelessWidget {
  final String message;
  final Function onPressed;
  const ErrorMessageWidget(
      {required this.onPressed, required this.message, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 4.w),
          padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: AppColors.getBlackGrey(),
              borderRadius: BorderRadius.circular(5.w)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(
                image: const AssetImage(AppAssets.errorIcon),
                height: 25.h,
                width: 40.w,
              ),
              Text(
                message,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: AppColors.getGreyWhite(),
                    fontSize: 16.sp,
                    fontWeight: FontWeight.bold),
              ),
              MaterialButton(
                color: AppColors.getPrimarySeconedary(),
                onPressed: () {
                  onPressed();
                },
                child: Text(
                  "Try again",
                  style: TextStyle(
                      color: AppColors.getWhiteBlack(),
                      fontSize: 14.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
