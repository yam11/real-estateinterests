import 'package:senior/Core/Constants/app_strings.dart';

import 'SharedPreferencesProvider.dart';

class AppSharedPreferences {
  static SharedPreferencesProvider? sharedPreferencesProvider;
  static init() async {
    sharedPreferencesProvider = await SharedPreferencesProvider.getInstance();
  }

  //token
  static String get getToken =>
      sharedPreferencesProvider!.read(AppStrings.token) ?? '';
  static saveToken(String value) =>
      sharedPreferencesProvider!.save(AppStrings.token, value);
  static bool get hasToken =>
      sharedPreferencesProvider!.contains(AppStrings.token);
  static removeToken() => sharedPreferencesProvider!.remove(AppStrings.token);

  //lang
  static String get getArLang =>
      sharedPreferencesProvider!.read(AppStrings.ar) ?? AppStrings.en;
  static saveArLang(String value) =>
      sharedPreferencesProvider!.save(AppStrings.ar, value);
  static bool get hasArLang =>
      sharedPreferencesProvider!.contains(AppStrings.ar);
  static removeArLang() => sharedPreferencesProvider!.remove(AppStrings.ar);

  static String get getUserName =>
      sharedPreferencesProvider!.read(AppStrings.userName) ?? "";
  static saveUserName(String value) =>
      sharedPreferencesProvider!.save(AppStrings.userName, value);
  static bool get hasUserName =>
      sharedPreferencesProvider!.contains(AppStrings.userName);
  static removeUserName() =>
      sharedPreferencesProvider!.remove(AppStrings.userName);
  //lang
  static bool get getDarkTheme =>
      sharedPreferencesProvider!.read(AppStrings.darkTheme) ?? false;
  static saveDarkTheme(bool value) =>
      sharedPreferencesProvider!.save(AppStrings.darkTheme, value);
  static bool get hasDarkTheme =>
      sharedPreferencesProvider!.contains(AppStrings.darkTheme);
  static removeDarkTheme() =>
      sharedPreferencesProvider!.remove(AppStrings.darkTheme);
}
