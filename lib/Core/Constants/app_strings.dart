class AppStrings {
  static const String token = "Token";
  static const String en = 'en';
  static const String ar = 'ar';
  static const String darkTheme = 'Dark Theme';
  static const String userName = "";
}
