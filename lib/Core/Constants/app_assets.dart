class AppAssets {
  //images
  static const String ii = 'assets/images/splash.jpg';
  static const String splashImage =
      'assets/images/Real_Estate_Interist__1_-removebg-preview.png';
  static const String welcomeImage = 'assets/images/house.png';
  //icons
  static const String lockIcon = 'assets/icons/lock.png';
  static const String emailIcon = 'assets/icons/email_icon.png';
  static const String errorIcon = 'assets/icons/warning.png';
  static const String phoneIcon = 'assets/icons/phone-call.png';
  static const String personIcon = 'assets/icons/user.png';
}
