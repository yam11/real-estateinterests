import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';

class AppColors {
  static const Color primaryColor = Color(0xFF857903);
  static const Color seconedaryColor = Color(0xFFB9BF18);
  static const Color blackLightColor = Color(0xFF1A1A1A);
  static const Color blackColor = Color(0xFF000000);


  static const Color greyColor = Color(0xFF707070);
  static const Color white2Color = Color(0xFFE6E6E6);
   static const Color whiteColor = Color(0xFFFFFFFF);
  static Color getPrimarySeconedary() =>
      AppSharedPreferences.hasDarkTheme ? primaryColor : seconedaryColor;
  static Color getSeconedaryPrimary() =>
      AppSharedPreferences.hasDarkTheme ? seconedaryColor : primaryColor;
  static Color getBlackLightWhite() =>
      AppSharedPreferences.hasDarkTheme ? blackLightColor : white2Color;
        static Color getWhiteGrey() =>
      AppSharedPreferences.hasDarkTheme ? white2Color : greyColor;
           static Color getWhiteBlack() =>
      AppSharedPreferences.hasDarkTheme ? white2Color : blackColor;
      static Color getBlackGrey() =>
      AppSharedPreferences.hasDarkTheme ? blackColor : greyColor;
  static Color getLightBlackgrey() =>
      AppSharedPreferences.hasDarkTheme ? blackLightColor : greyColor;
       static Color getGreyWhite() =>
      AppSharedPreferences.hasDarkTheme ?   greyColor:white2Color;
        static Color boxShadowColor() =>
      AppSharedPreferences.hasDarkTheme ?   greyColor:blackColor;
        static Color textColor() =>
      AppSharedPreferences.hasDarkTheme ?   white2Color:blackColor;
            static Color getBlackWhite() =>
      AppSharedPreferences.hasDarkTheme ?   blackColor:whiteColor;
}
